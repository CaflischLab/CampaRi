---
title: "MST and SST. The way to SAPPHIRE"
author: "Davide Garolini"
date: "10 Jan 2018"
vignette: >
  %\VignetteEngine{knitr::knitr}
  %\VignetteIndexEntry{MST_SST_vignette}
  %\usepackage[utf8]{inputenc}
---

```{r setup, include=FALSE}
if(!file.exists("to_d")) dir.create("to_d")
knitr::opts_chunk$set(echo = TRUE, eval = FALSE, warning = FALSE, results = TRUE, fig.width = 6, fig.height = 4.5)
# knitr::opts_knit$set(progress = FALSE)
knitr::opts_knit$set(root.dir="to_d/")
options(bitmapType="cairo")
```

This tutorial is focused on the SAPPHIRE plot and its possible clustering and visualization properties within the package CampaRi.

## MST
We will use here the trajectory generated using the following command (needs original campari package):
```{r run_nbu_sim, eval = FALSE}
cat("STARTING VIGNETTE MST_SST_vignette...\n")
library(CampaRi)
# standard run of the simulation in tutarial 11 - In this case we copy pasted the nbu_simulation.key
data.table::fwrite(list("NBU", "END"), file = "nbu.in", sep = "\n")
run_campari(FMCSC_SEQFILE="nbu.in",# you must have it defined according to CAMPARI's rules
            # FMCSC_BASENAME="NBU", # lets try the base_name option
            base_name = "NBU", print_status = F,  silent = F, print_log_error = T,
            PARAMETERS="oplsaal.prm", # if this variable it is not supplied will be automatically assigned to <full path to folder>/campari/params/abs3.2_opls.prm
            FMCSC_SC_IPP=0.0,
            FMCSC_SC_BONDED_T=1.0,
            FMCSC_DYNAMICS=3,
            FMCSC_FRICTION=3.0,
            FMCSC_TIMESTEP=0.005,
            FMCSC_TEMP=400.0,
            FMCSC_NRSTEPS=1000,
            FMCSC_EQUIL=0,
            FMCSC_XYZOUT=1,
            FMCSC_XYZPDB=3,
            FMCSC_TOROUT=1,
            FMCSC_COVCALC=20000000,
            FMCSC_SAVCALC=20000000,
            FMCSC_POLCALC=20000000,
            FMCSC_RHCALC=20000000,
            FMCSC_INTCALC=20000000,
            FMCSC_POLOUT=20000000,
            FMCSC_ENSOUT=20000000,
            FMCSC_ENOUT=20000000,
            FMCSC_RSTOUT=20000000
)
# rename
file.rename("NBU_traj.dcd", to = "NBU.dcd")
file.rename("FYC.dat", to = "NBU.fyc")
```

When loading a trajectory into R, run_campari() will use the ASCII support (producing a .tsv file) which is not able to select dihedral angles as the original run. Therefore we load directly NBU.fyc and use it (distance 1).

```{r mst_run_campari, eval = FALSE}
library(data.table)
# to use ncminer we need to load fyc directly (dihedral angles handling not implemented)
trj <- data.table::fread("NBU.fyc", header = F, skip = 1, data.table = FALSE)[,-1]
# head(trj)
# fread("head -n 1 NBU.fyc") # head of it
trj <- sapply(trj, as.numeric) # always be sure that it is numeric!
hist(trj[,2]) # this should have 3 peaks per diheadral angle
str(trj)
run_campari(trj = trj, base_name = "ascii_based_analysis",
            FMCSC_CPROGINDMODE=1, #mst
            FMCSC_CCOLLECT=1, print_status = F, silent = T, print_log_error = T, 
            FMCSC_CMODE=4,
            FMCSC_CDISTANCE=1,
            FMCSC_CPROGINDSTART=21, #starting snapshot 
            FMCSC_CRADIUS=10000,
            FMCSC_CCUTOFF=10000,
            FMCSC_CPROGINDWIDTH=1000) #local cut is automatically adjusted to 1/10 if it is too big (as here)
            #FMCSC_CPROGMSTFOLD 4 # b)
sapphire_plot(sap_file = "PROGIDX_000000000021.dat", timeline= T,title = "ORIGINAL CAMPARI - MST")
```

There is the possibility to use some internal replica of the main algorithms used for the metastable states recognition (basins in SAPPHIRE plots). The Minimum Spanning Tree, however, has memory limitation which are difficult to handle in R.

```{r, eval=F}
# netcdf dumping is broken with the new versions of netcdf (> 4.1)
# with 100k it is possible that also the netcdf option could exceede the memory availability
mst_from_trj(trj = trj, dump_to_netcdf = T, mode = "fortran", distance_method = 1, silent = T)
ret <- gen_progindex(nsnaps = nrow(trj), read_from_netcdf = T, snap_start = 21, silent = T); cat(nrow(trj), "\n")
ret2 <- gen_annotation(ret, snap_start = 21, local_cut_width = 200, silent = T)
sapphire_plot(sap_file = 'REPIX_000000000021.dat', timeline = T,title = "CAMPARI WRAPPER - MST")
```


It is however possible to use directly the R data system without the netcdf dumping (of the minimum spannin tree). NOTE: the installation can change this behaviour.
Let's see an example:
```{r, eval = FALSE}
adjl <- mst_from_trj(trj=trj, silent = T)
# adjl2 <- contract_mst(adjl = adjl, n_fold = 5)
ret <- gen_progindex(adjl = adjl, snap_start = 21, silent = T)
# ret <- gen_progindex(nsnaps = 2000, read_from_netcdf = T, snap_start = 21)
ret2 <- gen_annotation(ret_data = ret, local_cut_width = 200, snap_start = 21, silent = T)
sapphire_plot(sap_file = 'REPIX_000000000021.dat', timeline = T, title = "CAMPARI WRAPPER - MST")
```


It is also possible to use the SST which is a faster version to construct the spanning tree for the SAPPHIRE plot. The code in R was not updated to the latest version and was not reliable. For these reasons, we adopted a novel version which is only an interface/wrapper of the original code.

```{r, eval = FALSE}
campari_SST(trj, base_name = "basename", sap_file = "sap_file.dat", silent = T, multi_threading = F, rm_extra_files = T, 
            cmax_rad = NULL, cradius = NULL, pcs = NULL, state.width = 300, cdistance = 1, pi.start = 21, search.attempts = NULL, birchheight = NULL,
            leaves.tofold = NULL)
sapphire_plot(sap_file = "sap_file.dat", timeline= T,title = "ORIGINAL CAMPARI - MST")
```



let's do everything again with .xtc (starting from simulation even):

```{r, eval = FALSE}
# standard run of the simulation in tutarial 11 - In this case we copy pasted the nbu_simulation.key
run_campari(seq_in = "nbu.in",# you must have it defined according to CAMPARI's rules
            # FMCSC_BASENAME="NBU", # lets try the base_name option
            base_name = "NBU", print_status = F, print_log_error = T, # it will take 55 s in background ~
            PARAMETERS="oplsaal.prm", # if this variable it is not supplied will be automatically assigned to <full path to folder>/campari/params/abs3.2_opls.prm
            FMCSC_SC_IPP=0.0,
            FMCSC_SC_BONDED_T=1.0,
            FMCSC_DYNAMICS=3,
            FMCSC_FRICTION=3.0,
            FMCSC_TIMESTEP=0.005,
            FMCSC_TEMP=400.0,
            FMCSC_NRSTEPS=1000000,
            FMCSC_EQUIL=0,
            FMCSC_XYZOUT=100,
            FMCSC_XYZPDB=4, # xtc
            FMCSC_TOROUT=100,
            FMCSC_COVCALC=20000000,
            FMCSC_SAVCALC=20000000,
            FMCSC_POLCALC=20000000,
            FMCSC_RHCALC=20000000,
            FMCSC_INTCALC=20000000,
            FMCSC_POLOUT=20000000,
            FMCSC_ENSOUT=20000000,
            FMCSC_ENOUT=20000000,
            FMCSC_RSTOUT=20000000
)
# rename
# file.rename()
# system('mv NBU_traj.xtc NBU.xtc')
# system('mv FYC.dat NBU.fyc')
```


Now let's use classical campari to analyze it

```{r, eval=F}

run_campari(data_file = "NBU.xtc", nsnaps = 10000, base_name = "xtc_based_analysis",
            FMCSC_SEQFILE = "nbu.in",
            FMCSC_CPROGINDMODE=1, #mst
            FMCSC_CCOLLECT=1, print_status = T, print_log_error = T,
            FMCSC_CMODE=4,
            FMCSC_CDISTANCE=7, #rmsd without alignment 7 - dihedral distances need a complete analysis (pdb_format dcd pdb etc...) 
            FMCSC_CPROGINDSTART=22, #starting snapshot 
            # FMCSC_CPROGINDRMAX=1000, #search att
            # FMCSC_BIRCHHEIGHT=2, #birch height
            FMCSC_CMAXRAD=6, #clustering
            FMCSC_CRADIUS=4,
            FMCSC_CCUTOFF=100,
            FMCSC_CPROGINDWIDTH=1000) #local cut is automatically adjusted to 1/10 if it is too big (as here)
            #FMCSC_CPROGMSTFOLD 4 # b)
run_campari(data_file = "NBU.xtc", nsnaps = 10000, base_name = "xtc_based_analysis",
            FMCSC_SEQFILE = "nbu.in",
            FMCSC_CPROGINDMODE=2, #mst
            FMCSC_CCOLLECT=1, print_status = T, print_log_error = T,
            FMCSC_CMODE=4,
            FMCSC_CDISTANCE=1, 
            FMCSC_CPROGINDSTART=29, #starting snapshot 
            FMCSC_CPROGINDRMAX=10000, #search att
            FMCSC_BIRCHHEIGHT=10, #birch height
            FMCSC_CMAXRAD=1, #clustering
            FMCSC_CRADIUS=0,
            FMCSC_CCUTOFF=100,
            FMCSC_CPROGRDEPTH=10,
            FMCSC_CPROGINDWIDTH=1000) #local cut is automatically adjusted to 1/10 if it is too big (as here)
            #FMCSC_CPROGMSTFOLD 4 # b)
sapphire_plot(sap_file = "PROGIDX_000000000029.dat", timeline= T, title = "ORIGINAL CAMPARI - MST")
```

```{r outsetup, include = FALSE, eval = FALSE}
system("rm ../to_d/*")
```
