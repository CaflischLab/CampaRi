---
title: "Alanine Dipeptide (with a Git primer)"
author: "clangi, dgarol"
date: "`r format(Sys.time(), '%d %B, %Y')`"
vignette: >
  %\VignetteIndexEntry{"Alanine Dipeptide"}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r setup, include=FALSE}
if(!file.exists("to_d")) dir.create("to_d")
knitr::opts_chunk$set(echo = TRUE, eval = FALSE, warning = FALSE, results = TRUE, fig.width = 6, fig.height = 4.5)
# knitr::opts_knit$set(progress = FALSE)
knitr::opts_knit$set(root.dir="to_d/")
options(bitmapType="cairo")
library(CampaRi)
library(ggplot2)
# library(reshape2) # warum
data_dir <- "../../inst/extdata/"
camp_exe <- "/home/dgarolini/projects/for_campari/apt_campari/campari-3.0.1/bin/x86_64/camp_ncminer"
```

If you are here, you did this if you recall:
```{bash git_go, eval=FALSE}
# register to gitlab
# fork the repository from github
git clone https://gitlab.com/barbero.mito92/CampaRi.git # to create your local copy
git status
# git checkout -b tutorials # in rstudio
git fetch # to take the mods to the tutorial
git config user.email "barbero.mito92@gmail.com" # define the account using this repository
```


NB: AT THE MOMENT THE PIPELINE IS UNDER UPDATE AND THE FILES REFERRED WILL BE BROKEN LINKS. PLEASE WAIT FOR THE COMPLETION OF THE PROCESS


In this tutorial we will learn how to use CampaRi (R package) as an interface to Campari Fortran code in order to run and analyse a 
Molecular Dynamics simulation. For the sake of simplicity we will work with alanine dipeptide which is a commonly used toy model, as its torsional dynamics
can be represented by two degrees of freedom, *i.e.* the PHI and PSI angles.

## Introduction

Beforehands, we want to understand how do vignette work. As you can see it is a mix between markdown and blocks of R code. Who is familiar with python's notebooks will find it similar. This can be run line per line (CTRL + ENTER) or blockwise (CTRL + SHIFT + ENTER). We highlight these two possibilities as they refer to different working environments, *i.e.* one for the rstudio envirnment and one for the global run of the vignette. To see how it works in the end, knit it all! (CNTRL + SHIFT + K). 
Now, let's see the two different environments (depends on the setup block, of course, as it defines the environment on the run). PS: try to do the same on the console downstairs.

```{r env, eval=FALSE}
setwd("..")
cat(getwd())
```
So pay attention to the env, Always check the blocks variables and working directory.

---

Of course, the original campari-fortran software suite will be needed for this tutorial. Let's install it in a local folder.

```{r install_campary, eval=FALSE}
getwd()
dir.create("ori_campari")
install_campari(installation_location = "ori_campari", install_ncminer = T, no_optimization = T)
campari_exe <- "to_d/ori_campari/debcampari-master/bin/x86_64/camp_ncminer"
campari_exe <- "to_d/ori_campari/debcampari-master/bin/x86_64/campari"
```

## Generating a trajectory
In order to generate a trajectory with Campari we need an input key-file which describes the system setup and the parameters for the simulation.
An example key file is the following; this will run a Molecular Dynamics simulation of alanine dipeptide in torsional space for a total of 4 ns (This should take less than one minute on a modern laptop).

```{bash key_file, eval=FALSE}
  FMCSC_BBSEGFILE /home/clangini/software/campariv3_stable/data/bbseg2.dat    # lookup table for secondary structure measures
  PARAMETERS /home/clangini/software/campariv3_stable/params/abs3.2_opls.prm  # file defining system energies
  FMCSC_SEQFILE alanine_dipeptide.seq                                  # input file that defines the system
  
# Simulation Settings ##############################################################################
  FMCSC_BASENAME ala_dip  

  FMCSC_PDBANALYZE 0       # Run a fresh simulation of the system

  FMCSC_SHAPE 2            # Shape of simulation cell
  FMCSC_SIZE 400           # Dimensions of simulation cell
  FMCSC_BOUNDARY 4         # Boundary conditions

  FMCSC_NRSTEPS 1000 #10000000   # Total steps
  FMCSC_EQUIL   0      # Number of steps discarded as equilibration period

  #FMCSC_DYNAMICS 3         # Choice of system sampler
  #FMCSC_TIMESTEP 0.005 # ps
  #FMCSC_FRICTION 3.0 # 1/ps

  FMCSC_ENSEMBLE 1
  FMCSC_DYNAMICS 2
  FMCSC_TIMESTEP 0.004
  FMCSC_TSTAT 2
  FMCSC_TSTAT_TAU 1.0

  FMCSC_TMD_INTEGRATOR 1
  FMCSC_TMD_INT2UP 4


  FMCSC_TEMP 400.0         # Simulation Temperature

  FMCSC_RANDOMIZE 1        # Initial structure generation

# Hamiltonian ######################################################################################
  
  FMCSC_UAMODEL 0          # Logical to toggle building of aliphatic hydrogens
 
  FMCSC_SIGRULE 1          # Type of mean used to calculate pairwise sigma
  FMCSC_EPSRULE 2          # Type of mean used to calculate pairwise epsilon
  FMCSC_SC_IPP 1.0         # Scales inverse power potential (Usually repulsive L-J)
  FMCSC_SC_ATTLJ 1.0       # Scales strength of dispersive L-J interactions
  FMCSC_SC_WCA 0.0         # Scaling of Weeks-Chandler-Andersen potential (Independent of L-J)

  FMCSC_MODE_14 1          # 1-4 interaction requirement
  FMCSC_FUDGE_ST_14 1.0    # Scales 1-4 electrostatic potentials
  FMCSC_FUDGE_EL_14 1.0    # Scales 1-4 steric/dispersive potentials

  FMCSC_SC_BONDED_B 0.0    # Scaling factor for bonded potentials
  FMCSC_SC_BONDED_A 1.0    # Scaling factor for angular potentials
  FMCSC_SC_BONDED_I 1.0    # Scaling factor for improper dihedral potentials
  FMCSC_SC_BONDED_T 1.0    # Scaling factor for torsional potentials
  FMCSC_SC_EXTRA 0.0       # Scaling factor for structural correction potentials (obselete)

  FMCSC_SC_POLAR 1.0       # Scaling factor for all polar interactions

  FMCSC_SC_IMPSOLV 1.0     # Scales the strength of the ABSINTH DMFI; values larger than zero enable ABSINTH
  FMCSC_SAVPROBE 2.5       # Radius of solvent (calculates thickness of relevant solvation layer)

  FMCSC_IMPDIEL 78.2       # The value of the dielectric coefficient of the implicit solvent

  FMCSC_FOSTAU 0.25        # Steepness of sigmoidal interpolation for solvation energy
  FMCSC_FOSMID 0.1         # Midpoint of sigmoidal interpolation for solvation energy
                           # The above terms used in solvation states for the DMFI
  FMCSC_SCRMODEL 2         # Screening model for ABSINTH electrostatics
  FMCSC_SCRTAU 0.5         # Steepness of sigmoidal interpolation for coloumbic screening
  FMCSC_SCRMID 0.9         # Midpoint of sigmoidal interpolation for coloumbic screening

  FMCSC_INTERMODEL 1       # Exclusion model for short-range interactions
  FMCSC_ELECMODEL 2        # Additional exclusion rules for short-range electrostatic interactions

  FMCSC_SC_ZSEC 0.0        # Scaling factor for global secondary structure bias
  FMCSC_SC_DSSP 0.0        # Scaling factor for DSSP aligning potential (using H/E-Scores)
  FMCSC_SC_TOR 0.0         # Scaling factor controlling external torsional bias terms
  FMCSC_SC_DREST 0.0       # Scaling factor for externally defined harmonic distance restraints
  FMCSC_SC_TABUL 0.0       # Scaling factor for externally defined tabulated potentials
  FMCSC_SC_POLY 0.0        # Scaling factor for restraint potentials on polymeric properties (t/Î´)
  FMCSC_GHOST 0            # Whether to use interaction scaling ("ghosting")

# Verbose Reports ##################################################################################

  FMCSC_SEQREPORT 1        # Prints out a summary of sequence features
  FMCSC_DIPREPORT 1        # Prints out a summary of determined charge groups
  FMCSC_VDWREPORT 1        # Prints out a summary of van der Waals parameters
  FMCSC_FOSREPORT 1        # Prints out a summary of free energies of solvation 
  FMCSC_BONDREPORT 1       # Prints out a summary of bonded potentials
  FMCSC_ELECREPORT 1       # Prints out a summary of close-range electrostatics
  FMCSC_INTERREPORT 1      # Prints out a summary of short-range interactions

# Output Files #####################################################################################

  FMCSC_BASENAME ala_dipep       # Basename used before any output files
  FMCSC_DISABLE_ANALYSIS 	1
  FMCSC_XYZOUT 1000             # Writes out coordinates to trajectory file
  FMCSC_TOROUT 1000		# Data collection for FYC

  FMCSC_XYZPDB  3                # Outputs trajectory information as an .dcd file
  FMCSC_XYZMODE 1               # Outputs the trajectory as a single file

# Varying Parameters ################################################################################
  FMCSC_NRTHREADS 4
```

This key-file can be read directly in an R session using the keywords_from_keyfile() function. Please remember that the above formatting should be stricly followed (the commented and empty lines are automatically discarded).

```{r, eval=FALSE}
data_dir <- "../../inst/extdata/"
getwd()
#setwd('to_d')
# reading an already formatted keyfile (it will skip the empty lines and comments)
keywords <- keywords_from_keyfile(key_file_input = paste0(data_dir, "alanine_dipeptide.key")) # list format (keywords_names = values)
keywords2 <- keywords_from_keyfile(key_file_input = paste0(data_dir, "alanine_dipeptide.key"), return_string_of_arguments = TRUE) # copy paste format
```

run_campari() function is a wrapper of Campari which allows to call the original Campari Fortran code directly from R. In its basic functionality 
it can mimick a Campari call by requiring only a key-file as input:
```{r, eval=FALSE}
run_campari(campari_exe = campari_exe, key_file_input = paste0(data_dir, "alanine_dipeptide.key"),
            seq_in=paste0(data_dir, "alanine_dipeptide.seq"))
```

However a more interesting possibility is to read a key-file and directly add modifications to it in R. We can use the function do.call as 
the keywords for Campari have to be specified as a list. In the following we are simply overriding the basename specified in the original key 
file *alanine_dipeptide.key*.

```{r, eval=FALSE}
do.call(run_campari, c(campari_exe = campari_exe, 
                       as.list(keywords), FMCSC_BASENAME="my_new_basename", 
                       FMCSC_SEQFILE=paste0(data_dir, "alanine_dipeptide.seq")))
```

## Basic trajectory analysis:
We can start analysing our system by plotting some geometric qunatities which could help us in better understand its dynamical behaviour.
Alanine dipeptide is often used as a toy model as it has single PHI and PSI angles which can be plotted on a Ramachandran plot. 
The values of all dihedral angles in the system (for polypeptides these are phi, psi, omega and chi sidechain torsions) are calculated and dumped by Campari 
to the file *FYC.dat*, with the frequency specified by **FMCSC_TOROUT**.

As Campari writes out all the dihedrals, we want to select only the ones we are interested in (in our case the PHI and PSI angles of alanine dipeptide). 
We can do this by having a look at the parsed header of the **FYC.dat** file.

```{r, eval=FALSE}
wkdir <- getwd() # working directory
cat(wkdir)
fyc_header <- extract_fyc_header(paste0(wkdir, "/FYC.dat")) # read header of FYC.dat
print(fyc_header)
```
Now we can load the FYC.dat file and visualize the features by means of density plots or the Ramachandran plot.
In the Ramachandran plot we color the points according to the absolute sampling time.

```{r, eval=FALSE}
fyc_dat <- read.table(paste0(wkdir, "/FYC.dat"))
fyc_dat <- setNames(fyc_dat, fyc_header)
p1 <- ggplot(fyc_dat) + geom_density(aes(x=ALA_1_PHI), fill='lightblue')
p2 <- ggplot(fyc_dat) + geom_density(aes(x=ALA_1_PSI), fill='lightblue')
p <- cowplot::plot_grid(p1, p2, ncol=2)
print(p)
```
```{r, eval=FALSE}
# Ramachandran plot:
p1 <- ggplot(fyc_dat) + geom_point(aes(x=ALA_1_PHI, y=ALA_1_PSI, color=Step)) + 
  scale_color_gradientn(colours = rainbow(5))
# and corresponding 2D density:
p2 <- ggplot(fyc_dat) + stat_density2d(aes(x=ALA_1_PHI, y=ALA_1_PSI, fill=stat(nlevel)), geom="polygon") +
  scale_x_continuous(limits = c(-180, 180)) + scale_y_continuous(limits = c(-180, 180))

p <- cowplot::plot_grid(p1, p2, ncol=2)
print(p)
```

The PHI and PSI angles appears to be good features to describe the slow dynamical modes of the system; 
In particular they can very well describe the transition to the left-handed alpha-helical region.  
However let us consider a bigger set of 
(possibly) correlated features, for example a set of interatomic distances.
Just as an example we use all the possible interatomic distances between any pairs of heavy atoms. 
To do so we need to retrieve the index of the heavy atoms from a CAMPARI-generated pdb file and write the index file to specify for the keyword FMCSC_PCCODEFILE.
For more information about the format of the PCCODEFILE please refer to [http://campari.sourceforge.net/V3/inputfiles.html](http://campari.sourceforge.net/V3/inputfiles.html).
```{r, eval=FALSE}
ha_idx <- c(1,2,3,7,8,9,10,13,17,18) # index of heavy atoms in the pdb
pcs <- combn(ha_idx, m=2, simplify = T) # enumerate all possible combinations
n_pcs <- ncol(pcs)
pccode_fn <- "ala_ha.pccodefile"
# write index file:
write(x = c(1, n_pcs), file = pccode_fn, ncolumns = 2)
write(t(cbind(t(pcs),1)), file = pccode_fn, ncolumns = 3, , append=TRUE)

# run CAMPARI:
run_campari(base_name = "ala2_gpc", print_status = F,
            data_file="ala2_traj.xtc", 
            PARAMETERS="abs3.2_opls.prm", # parameters
            FMCSC_SEQFILE="alanine_dipeptide.seq", # input file that defines the system
            FMCSC_PDBANALYZE=1, # Run an analysis
            FMCSC_PDB_TEMPLATE="ala2_START.pdb", 
            FMCSC_SHAPE=1,            # Shape of simulation cell
            FMCSC_SIZE="40.0  40.0  40.0",          # Dimensions of simulation cell
            FMCSC_ORIGIN="0.0   0.0   0.0",
            FMCSC_BOUNDARY=1,         # Boundary conditions
            FMCSC_NRSTEPS=100000,
            FMCSC_EQUIL=0,
            FMCSC_PDB_FORMAT=3,
            FMCSC_SC_IPP=0.0,
            FMCSC_SEQREPORT=1,        # Prints out a summary of sequence features
            FMCSC_DISABLE_ANALYSIS=1,
            FMCSC_PCCALC=1,
            FMCSC_INSTGPC=1,
            FMCSC_PCCODEFILE="ala_ha.pccodefile")
```

The extracted distances are written to the file `GENERAL_DIS.dat`.

<!---
```{r, eval=FALSE}
feats <- data.table::fread(paste0(data_dir,'GENERAL_DIS_250ns.dat'), skip = 2, data.table = F)
nfeats <- ncol(feats) - 1 # first column is the step
```
-->

These features are by construction redundant, so we would like to explore different possibilities of dimensionality reduction.
The first method we can use is Principal Component Analysis (PCA), which however it might not be the most appropriate choice to retain information about the slow dynamics of our model, because it relies on finding the direction of maximum variance (in terms of a purely geometric distance) without making use of any kinetic information.

```{r, eval=FALSE}
con = file(paste0(data_dir, 'PRINCIPAL_COMPONENTS_pca_250ns.dat'),open="r")
raw_header = readLines(con, n = 1)
close(con)
pca_variances <- as.numeric(unlist(strsplit(gsub('# ', '', raw_header), "\\s+")))
p <- ggplot(data.frame(x=1:nfeats, y=pca_variances)) + geom_point(aes(x=x, y=y))
p
p <- ggplot(data.frame(x=1:nfeats, y=cumsum(pca_variances)/sum(pca_variances))) + geom_point(aes(x=x, y=y)) +
  geom_hline(yintercept=0.9, linetype="dashed")
p
pca_dat <- data.table::fread(paste0(data_dir, 'PRINCIPAL_COMPONENTS_pca_250ns.dat'), skip = 1, data.table = F)

df <- pca_dat
df$step <- 1:250000
p <- ggplot(df) + geom_point(aes(x=V1, y=V2, color=step)) + 
  scale_color_gradientn(colours = rainbow(5))
print(p)
p <- ggplot(pca_dat) + stat_density2d(aes(x=V1, y=V2, fill=stat(level)), geom="polygon") #+
  #scale_x_continuous(limits = c(-180, 180)) + scale_y_continuous(limits = c(-180, 180))
print(p)

```

We can try to give an interpretation of the principal components in terms of the original features. In order to do this we can
plot the loadings (the weight of each original feature in the principal component). These are the eigenvectors of the covariance matrix scaled by
the corresponding eigenvalues. Here we print the loadings for the first 3 components.

```{r, eval=FALSE}
pca_eigenvec <- data.table::fread(paste0(data_dir,'PRINCIPAL_COMPONENTS_pca.evs'), skip = 1, data.table = F)
pca_loadings <- pca_eigenvec[,1:3] * diag(pca_variances[1:3])

df <- cbind(1:nfeats, pca_loadings)
names(df) <- c("y", "V1", "V2", "V3")
meltone <- melt(df, id.vars = "y")
p <- ggplot(meltone) + geom_tile(aes(y=y, x=variable, fill=value)) + 
  scale_fill_gradient2()
print(p)
```

We can now do the same analysis with TICA, which tries to find independent components by maximizing their autocorrelation.

```{r, eval=FALSE}
con = file(paste0(data_dir,'PRINCIPAL_COMPONENTS_tica_250ns.dat'),open="r")
raw_header = readLines(con, n = 1)
close(con)
tica_variances <- as.numeric(unlist(strsplit(gsub('#', '', raw_header), "\\s+")))
p <- ggplot(data.frame(x=1:nfeats, y=tica_variances)) + geom_point(aes(x=x, y=y))
p
p <- ggplot(data.frame(x=1:nfeats, y=cumsum(tica_variances^2)/sum(tica_variances^2))) + geom_point(aes(x=x, y=y)) +
  geom_hline(yintercept=0.9, linetype="dashed")
p
tica_dat <- data.table::fread(paste0(data_dir,'PRINCIPAL_COMPONENTS_tica_250ns.dat'), skip = 1, data.table = F)

df <- tica_dat
df$step <- 1:250000
p <- ggplot(df) + geom_point(aes(x=V1, y=V2, color=step)) +
  scale_color_gradientn(colours = rainbow(5))
print(p)
p <- ggplot(tica_dat) + stat_density2d(aes(x=V1, y=V2, fill=stat(level)), geom="polygon") #+
  #scale_x_continuous(limits = c(-180, 180)) + scale_y_continuous(limits = c(-180, 180))
print(p)
```
```{r, eval=FALSE}
tica_eigenvec <- data.table::fread(paste0(data_dir,'PRINCIPAL_COMPONENTS_tica.evs'), skip = 1, data.table = F)
tica_loadings <- tica_eigenvec[,1:2] * diag(tica_variances[1:2])

df <- cbind(1:nfeats, tica_loadings)
names(df) <- c("y", "V1", "V2")
meltone <- melt(df, id.vars = "y")
p <- ggplot(meltone) + geom_tile(aes(y=y, x=variable, fill=value)) + 
  scale_fill_gradient2()
print(p)
```

## Markov State Model
We can use the extracted PCA or TICA features to build a Markov State Model (MSM) to study the dynamics of alanine dipeptide.
The first step to build a network model consists in discretizing the trajectory by means of clustering. We will use the tree-based clustering of Vitalis et al. for this purpose as it 
conserves the dynamics of the system fairly well.
The most important parameters of the tree-based clustering are CRADIUS and CMAXRAD; these will determine how fine the clustering is.
To have a good starting estimate we can use `find_perc_TBC` which estimates the percentiles of the distance spectrum. NOTE: In the latest version of Campari this information is 
printed automatically to the log file.

```{r, eval=FALSE}
pca_percentiles <- find_perc_TBC(pca_dat[,1:2], perc=c(0.05, 0.85), meth = "rmsd", n_sampling = 1000)
tica_percentiles <- find_perc_TBC(tica_dat[,1:2], meth = "rmsd")

clu_dat_pca <- campari_TBC(camp_exe = camp_exe, pca_dat[,1:2], cmax_rad=pca_percentiles[2], cradius=pca_percentiles[1], 
                       birchheight=8, birchmulti=3, cdistance=7, pca_mode = "none", base_name = "pca_tbc",
                        silent = F, multi_threading = F)
# get the list of size and resolutions from the clustering:
res_df <- get_TBC_resolutions('pca_tbc.log')
pca_centroids <- get_TBC_centroids('pca_tbc.log', as.numeric(res_df$res[1]))
```

We visualize the result of the clustering, reproducing the previous scatter plot coloring by cluster.
We can also add the centroid of each cluster to the plot.
It is also interesting to see how clusters generated by PCA are mapped on the original Ramachandran plot.

```{r, eval=FALSE}
df <- data.frame(PC1=pca_dat[ ,1], PC2=pca_dat[ ,2], clu=as.factor(clu_dat_pca$clu[ ,1]))
df_centroid <- df[pca_centroids$center,]
p <- ggplot(df) + geom_point(aes(x=PC1, y=PC2, color=clu)) + 
  geom_point(data=df_centroid, aes(x=PC1, y=PC2), color="black", size=2) + 
  theme(legend.position = "none")
print(p)

df <- data.frame(phi=fyc_dat$ALA_1_PHI, psi=fyc_dat$ALA_1_PSI, clu=as.factor(clu_dat_pca$clu[ ,1]))
p <- ggplot() + geom_point(data=df, aes(x=phi, y=psi, color=clu)) +
  geom_point(data=df[pca_centroids$center,], aes(x=phi, y=psi), color='black', size=2) +
  theme(legend.position = "none")
print(p)
```

We do now the same procedure for TICA.
```{r, eval=FALSE}
tica_percentiles <- find_perc_TBC(tica_dat[,1:2], perc=c(0.04, 0.85), meth = "rmsd", n_sampling=1000)

clu_dat_tica <- campari_TBC(camp_exe = camp_exe, tica_dat[,1:2], cmax_rad=tica_percentiles[2], cradius=tica_percentiles[1], 
                       birchheight=8, birchmulti=3, cdistance=7, pca_mode = "none", base_name = "tica_tbc",
                        silent = F, multi_threading = F)
# get the list of size and resolutions from the clustering:
res_df <- get_TBC_resolutions('tica_tbc.log')
tica_centroids <- get_TBC_centroids('tica_tbc.log', as.numeric(res_df$res[2]))
```
```{r, eval=FALSE}
df <- data.frame(IC1=tica_dat[ ,1], IC2=tica_dat[ ,2], clu=as.factor(clu_dat_tica$clu[ ,2])) # NOTE we use column 2 to have a similar number of clusters with respect to the tica case
p <- ggplot() + geom_point(data=df, aes(x=IC1, y=IC2, color=clu)) +
  geom_point(data=df[tica_centroids$center, ], aes(x=IC1, y=IC2), color="black", size=2) +
  theme(legend.position = "none")
print(p)

df <- data.frame(phi=fyc_dat$ALA_1_PHI, psi=fyc_dat$ALA_1_PSI, clu=as.factor(clu_dat_tica$clu[ ,2])) # NOTE we use column 2 to have a similar number of clusters with respect to the tica case
p <- ggplot() + geom_point(data=df, aes(x=phi, y=psi, color=clu)) +
  geom_point(data=df[tica_centroids$center, ], aes(x=phi, y=psi), color="black", size=2) +
  theme(legend.position = "none")
print(p)
```

After clustering, the second step to build an MSM is to count the transitions between states in order to infer the transition matrix.
This can be done using the `msm` function.

```{r, eval=FALSE}
msm_pca <- msm(seq=clu_dat_pca$clu[,1], lag = 10, tm.opt = "symm")
plot(msm_pca$values)
# main timescales
df <- data.frame(phi=fyc_dat$ALA_1_PHI, psi=fyc_dat$ALA_1_PSI, clu=as.factor(clu_dat_pca$clu[ ,1]))
df_centroid <- df[pca_centroids$center,]
df_centroid$t1 <- msm_pca$right[,2]
df_centroid$t2 <- msm_pca$right[,3]
df_centroid$t3 <- msm_pca$right[,4]
p <- ggplot(df_centroid) + geom_point(aes(x=phi, y=psi, color=t1), size = 2) +
  scale_x_continuous(limits = c(-180, 180)) + scale_y_continuous(limits = c(-180, 180)) +
  scale_color_gradient2(low="red", mid = "yellow", high="blue", midpoint = 0, space = "Lab")
print(p)
p <- ggplot(df_centroid) + geom_point(aes(x=phi, y=psi, color=t2), size = 2) +
  scale_x_continuous(limits = c(-180, 180)) + scale_y_continuous(limits = c(-180, 180)) +
  scale_color_gradient2(low="red", mid = "yellow", high="blue", midpoint = 0, space = "Lab")
print(p)
```
The same for TICA:
```{r, eval=FALSE}
msm_tica <- msm(seq=clu_dat_tica$clu[,2], lag = 10, tm.opt = "symm")
plot(msm_tica$values)
# main timescales
df <- data.frame(phi=fyc_dat$ALA_1_PHI, psi=fyc_dat$ALA_1_PSI, clu=as.factor(clu_dat_tica$clu[ ,1]))
df_centroid <- df[tica_centroids$center,]
df_centroid$t1 <- msm_tica$right[,2]
df_centroid$t2 <- msm_tica$right[,3]
df_centroid$t3 <- msm_tica$right[,4]
df_centroid$t4 <- msm_tica$right[,5]
df_centroid$t5 <- msm_tica$right[,6]
p <- ggplot(df_centroid) + geom_point(aes(x=phi, y=psi, color=t1), size = 2) +
  scale_x_continuous(limits = c(-180, 180)) + scale_y_continuous(limits = c(-180, 180)) +
  scale_color_gradient2(low="red", mid = "yellow", high="blue", midpoint = 0, space = "Lab")
print(p)
p <- ggplot(df_centroid) + geom_point(aes(x=phi, y=psi, color=t2), size = 2) +
  scale_x_continuous(limits = c(-180, 180)) + scale_y_continuous(limits = c(-180, 180)) +
  scale_color_gradient2(low="red", mid = "yellow", high="blue", midpoint = 0, space = "Lab")
print(p)
```
## Implied timescale
We can now compare the implied timescales and see if we are able to resolve the slow modes of the dynamics.

```{r, eval=FALSE}
# timescales pca
ntimescales <- 4
taus <- c(1, seq(10, 50, by=5))
ntaus <- length(taus)
timescales <- matrix(data = 0, nrow = ntaus, ncol = ntimescales)
for (itau in 1:ntaus){
  my_msm <- msm(seq=clu_dat_pca$clu[,1], lag = taus[itau], tm.opt = "symm", silent = T)
  timescales[itau, ] <- -taus[itau]/log(my_msm$values[2:(ntimescales+1)])
}
# plot
df <- as.data.frame(timescales)
df$tau <- taus
meltone <- melt(df, id = "tau")
p <- ggplot(meltone) + geom_line(aes(x = tau, y = value, color = variable))
print(p)
```
```{r, eval=FALSE}
# timescales pca
ntimescales <- 4
taus <- c(1, seq(10, 50, by=5))
ntaus <- length(taus)
timescales <- matrix(data = 0, nrow = ntaus, ncol = ntimescales)
for (itau in 1:ntaus){
  my_msm <- msm(seq=clu_dat_tica$clu[,1], lag = taus[itau], tm.opt = "symm", silent = T)
  timescales[itau, ] <- -taus[itau]/log(my_msm$values[2:(ntimescales+1)])
}
```
```{r, eval=FALSE}
# plot
df <- as.data.frame(timescales)
df$tau <- taus
meltone <- melt(df, id = "tau")
p <- ggplot(meltone) + geom_line(aes(x = tau, y = value, color = variable))
print(p)

p <- ggplot(meltone) + geom_line(aes(x = tau, y = value, color = variable)) +
 scale_y_continuous(limits = c(-180, 180))
print(p)
```
## Sapphire
Having a prototype SAPPHIRE plot is just a two-liner:

```{r, eval=FALSE}
campari_SST(fyc_dat[seq(1,250000, by = 10),3:4], base_name = "my_progind", sap_file = "my_sap_file.dat", 
            silent = F, multi_threading = F, 
            rm_extra_files = T,
            cmax_rad = NULL, cradius = NULL, pcs = NULL, state.width = 300, cdistance = 7, pi.start = 1,
            search.attempts = 1000, birchheight = NULL,
            leaves.tofold = NULL)
```
```{r, eval=FALSE}
sapphire_plot(sap_file = "my_sap_file.dat", timeline= T,title = "ALANINE DIPEPTIDE - SST")
```