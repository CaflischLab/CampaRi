# Common practices
Here we cover the decisions about style and development that we made and should be kept consistent in the future (and possibly retroactively changed).

* Saving plots depends on the single users. Any functions should only return the plot object.
* Naming should be lowercase and words should be separated by underscore.
* How to avoid CRAN check NOTE 'Undefined global functions or variables'. \
	In most of the cases it happens due to ggplot aestethetics or dplyr/tidyr columns.\
	The vast majority of these cases can be solved calling the variables through  `rlang::.data`.\
	Sometimes, however, also strings are enough *e.g.*, `select(mtcars, "mpg")`.
	See [dplyr vignette](https://cran.r-project.org/web/packages/dplyr/vignettes/programming.html) for more details on the use of `rlang::.data`.
