context('mahalanobis distance')

test_that('Using the pipeline for finding the mahalanobis distance', {
  
  # ------------------- =
  # setting up the test
  silent <- T
  plt_stff <- !silent
  withr::local_package("tictoc")
  
  # some cleaning for my visual appreciation of a thin concept of order
  withr::defer(.loop_file_rm(c("REPIX_000000000001.dat",
    "maha_f.dat", "basename.log", "basename.tsv", "MST_DUMPLING.nc",
    "MST_DUMPLING.nc", "maha_f.dat", "THREADS.log", list.files(pattern = "PI"), list.files(pattern = "STRUCT"),
    list.files(pattern = "dyn"), list.files(pattern = "dir"), list.files(pattern = "prog")
  )))
  
  # setting main exe folder
  main_dir <- system.file("extdata/for_campari/", package = "CampaRi")
  bin_dir <- paste0(main_dir, "/", dir(main_dir)[1], "/bin/")
  ca_exe <- paste0(bin_dir, "/", dir(bin_dir)[1])
  # ------------------- =
  
  # test mahalanobis distance
  n_elem <- 1000
  n_feats <- 5
  
  c1 <- matrix(rnorm(n_elem * n_feats), nrow = n_elem, ncol = n_feats)
  c2 <- matrix(rnorm(n_elem * n_feats), nrow = n_elem, ncol = n_feats)
  c3 <- matrix(rnorm(n_elem * n_feats), nrow = n_elem, ncol = n_feats)
  # c1 <- matrix(rbeta(n_elem*n_feats, shape1 = 0.5, shape2 = 0.5), nrow = n_elem, ncol = n_feats)
  # c2 <- matrix(rbeta(n_elem*n_feats, shape1 = 0.5, shape2 = 0.5), nrow = n_elem, ncol = n_feats)
  # c3 <- matrix(rbeta(n_elem*n_feats, shape1 = 0.5, shape2 = 0.5), nrow = n_elem, ncol = n_feats)
  c3[, 1:2] <- c3[, 1:2] + 1
  c2[, 3:5] <- c2[, 3:5] - 1
  c1[, 3:5] <- c1[, 3:5] - 1
  ann <- c(rep(1, n_elem * 2), rep(2, n_elem))
  if (plt_stff) {
    withr::local_package("tibble")
    withr::local_package("dplyr")
    withr::local_package("ggplot2")
  
    df <- cbind(as.data.frame(rbind(c1, c2, c3)), as.factor(c(rep("set1", n_elem), rep("set2", n_elem), rep("set3", n_elem))))
    colnames(df)[ncol(df)] <- "Cluster"
  
    df %>% as_tibble(.name_repair = c("unique"))
  
    aplot <- prcomp(as.matrix(df[, 1:(ncol(df) - 1)]))
  
    aplot$x %>%
      as_tibble(.name_repair = c("unique")) %>%
      select(1:2) %>%
      add_column("Cluster" = df$Cluster) %>%
      ggplot(aes(x = PC1, y = PC2, col = Cluster)) +
      geom_point()
  }
  
  # Dyn Prog maha finding ===================================================================================================================
  .ev_it(Xad <- find_mahaR(c1, c2, c3, K = 10, J = 5, tol = 10^(-5), C = 1, loss_f = "Hinge", silent = silent, line_search_alpha = T), s = silent)
  .ev_it(Xad <- find_mahaR(c1, c2, c3, K = 10, J = 5, tol = 10^(-5), C = 1, loss_f = "Huber", silent = silent, verbose = !silent, line_search_alpha = F), s = silent)
  
  # Prob Estimation maha ===================================================================================================================
  .ev_it(maha_like <- find_maha_likeRat(c1, c2, c3, silent = silent), s = silent)
  
  # from now on we use always the same starting snapshot (1)
  # My MST ===================================================================================================================
  
  # create MST - euclidean
  adjl <- mst_from_trj(trj = rbind(c1, c2, c3), distance_method = 5, return_tree_in_r = T, mute_fortran = T, silent = silent)
  ret <- gen_progindex(adjl, snap_start = 1, silent = silent)
  ret2 <- gen_annotation(ret, snap_start = 1, silent = silent)
  file.rename("REPIX_000000000001.dat", "PI_myMST_eucl.dat")
  
  # create MST - maha dyn prog
  .ev_it(adjl <- mst_from_trj(trj = rbind(c1, c2, c3), distance_method = 12, dump_to_netcdf = T, metric_mat = Xad, return_tree_in_r = T, mute_fortran = silent, silent = silent), s = silent)
  .ev_it(ret <- gen_progindex(adjl, snap_start = 1, silent = silent), s = silent)
  .ev_it(ret2 <- gen_annotation(ret, snap_start = 1, silent = silent), s = silent)
  file.rename("REPIX_000000000001.dat", "PI_myMST_mahaDynProg.dat")
  
  # create MST - maha prob estim
  .ev_it(adjl <- mst_from_trj(trj = rbind(c1, c2, c3), distance_method = 12, dump_to_netcdf = T, metric_mat = maha_like, return_tree_in_r = T, mute_fortran = silent, silent = silent), s = silent)
  ret <- gen_progindex(adjl, snap_start = 1, silent = silent)
  ret2 <- gen_annotation(ret, snap_start = 1, silent = silent)
  file.rename("REPIX_000000000001.dat", "PI_myMST_mahaProbEstim.dat")
  
  
  # Campari MST ===================================================================================================================
  # finding percentiles to be used by SST and original MST
  aaaa <- find_perc_TBC(rbind(c1, c2, c3), perc = c(0.99, 0.99), plot = TRUE, silent = TRUE)
  # fixing the executable
  camp_exe <- paste0(ca_exe, "/camp_ncminer")
  rm_extra_file_camp <- T # removing stuffs?
  
  # create MST (campari) - euclidean
  .ev_it(adjl <- campari_SST(
    trj = rbind(c1, c2, c3), pi.start = 1, cdistance = 7, rm_extra_files = rm_extra_file_camp,
    search.attempts = 3000, silent = silent, birchheight = 2, cmax_rad = 100, cradius = 100, leaves.tofold = 0, MST = T,
    camp_exe = camp_exe, run_in_background = F, multi_processing = F, FMCSC_CCUTOFF = 100
  ), s = silent)
  file.rename("PI_output_basename_pistart1.dat", "PI_campMST_euclNoMultiProc.dat")
  
  # create MST (campari) - euclidean multi_processing
  .ev_it(adjl <- campari_SST(
    trj = rbind(c1, c2, c3), pi.start = 1, cdistance = 7, rm_extra_files = rm_extra_file_camp, search.attempts = 3000, silent = silent,
    birchheight = 2, cmax_rad = 100, cradius = 100, leaves.tofold = 0, MST = T,
    camp_exe = paste0(camp_exe, "_threads"), run_in_background = F, multi_processing = T, FMCSC_CCUTOFF = 100
  ), s = silent)
  file.rename("PI_output_basename_pistart1.dat", "PI_campMST_euclMultiProc.dat")
  
  
  # Campari SST ===================================================================================================================
  # create SST - eucl w/o threads
  .ev_it(adjl <- campari_SST(
    trj = rbind(c1, c2, c3), pi.start = 1, cdistance = 7, rm_extra_files = rm_extra_file_camp, search.attempts = 3000, silent = silent, birchheight = 2, cmax_rad = 100, cradius = 100, leaves.tofold = 0,
    camp_exe = camp_exe, run_in_background = F, multi_processing = F, FMCSC_CCUTOFF = 100
  ), s = silent)
  file.rename("PI_output_basename_pistart1.dat", "PI_campSST_euclNoMultiProc.dat")
  
  # create SST - eucl w/ threads
  .ev_it(adjl <- campari_SST(
    trj = rbind(c1, c2, c3), pi.start = 1, cdistance = 7, rm_extra_files = rm_extra_file_camp, search.attempts = 3000, silent = silent, birchheight = 2, cmax_rad = 100, cradius = 100, leaves.tofold = 0,
    camp_exe = paste0(camp_exe, "_threads"), run_in_background = F, multi_processing = T, FMCSC_CCUTOFF = 100
  ), s = silent)
  file.rename("PI_output_basename_pistart1.dat", "PI_campSST_euclMultiProc.dat")

  # BUG IN THE ORIGINAL CAMPARI. Please refer to the issue  
  # # create SST - maha dyn w/ threads
  # data.table::fwrite(Xad, paste0(getwd(), "/maha_f.dat"), sep = " ", col.names = F)
  # .ev_it(adjl <- campari_SST(
  #   trj = rbind(c1, c2, c3), pi.start = 1, cdistance = 7, rm_extra_files = rm_extra_file_camp, search.attempts = 3000, silent = silent, birchheight = 2, cmax_rad = 100, cradius = 100, leaves.tofold = 0,
  #   camp_exe = paste0(camp_exe, "_threads"), run_in_background = F, multi_processing = T,
  #   FMCSC_NCDM_MAHAFI = paste0(getwd(), "/maha_f.dat"), FMCSC_CDISPECIAL = 55, FMCSC_CCUTOFF = 100
  # ), s = silent)
  # file.rename("PI_output_basename_pistart1.dat", "PI_campSST_mahaDynMultiProc.dat")
  # 
  # # create SST - maha dyn w/o threading
  # .ev_it(adjl <- campari_SST(
  #   trj = rbind(c1, c2, c3), pi.start = 1, cdistance = 7, rm_extra_files = rm_extra_file_camp, search.attempts = 3000, silent = silent, birchheight = 2, cmax_rad = 100, cradius = 100, leaves.tofold = 0,
  #   camp_exe = camp_exe, run_in_background = F,
  #   FMCSC_NCDM_MAHAFI = paste0(getwd(), "/maha_f.dat"), FMCSC_CDISPECIAL = 55, multi_processing = F, FMCSC_CCUTOFF = 100
  # ), s = silent)
  # file.rename("PI_output_basename_pistart1.dat", "PI_campSST_mahaDynNoMultiProc.dat")
  # 
  # # create SST - maha prob
  # data.table::fwrite(maha_like, paste0(getwd(), "/maha_f.dat"), sep = " ", col.names = F)
  # .ev_it(adjl <- campari_SST(
  #   trj = rbind(c1, c2, c3), pi.start = 1, cdistance = 7, rm_extra_files = rm_extra_file_camp, search.attempts = 3000, silent = silent, birchheight = 2, cmax_rad = 100, cradius = 100, leaves.tofold = 0,
  #   camp_exe = camp_exe, run_in_background = F,
  #   FMCSC_NCDM_MAHAFI = paste0(getwd(), "/maha_f.dat"), FMCSC_CDISPECIAL = 55, multi_processing = T, FMCSC_CCUTOFF = 100
  # ), s = silent)
  # file.rename("PI_output_basename_pistart1.dat", "PI_campSST_mahaProbMultiProc.dat")
   
})
