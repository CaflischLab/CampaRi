context('show_clustering_summary')

test_that('Test for showing the clustering summary and other details', {
  
  # ------------------- =
  # setting up the test
  silent <- F
  plt_stff <- !silent
  withr::local_package("tictoc")
  # ------------------- =
  
  file_fyc <- system.file("extdata", "NBU.fyc", package = "CampaRi")
  file_fyc2 <- system.file("extdata", "FYC_example.dat", package = "CampaRi")
  file_log <- system.file("extdata", "NBU.log", package = "CampaRi")
  
  # specific cluster option
  .ev_it(show_clustering_summary(log_file = file_log, fyc_file = file_fyc, which_cluster = 10, return_centers = T, silent = silent), NA)
  .ev_it(show_clustering_summary(log_file = file_log, fyc_file = file_fyc2, which_cluster = 10, return_centers = T, silent = silent), err = T)
  
  # first 11 clusters
  .ev_it(show_clustering_summary(log_file = file_log, fyc_file = file_fyc, which_first_clusters = 11, return_centers = T, silent = silent), NA)
  .ev_it(show_clustering_summary(log_file = file_log, fyc_file = file_fyc2, which_first_clusters = 11, return_centers = T, silent = silent), err = T)

  # @Cassiano, why I added this? ps: make use of the test files when you create a new feature! I also use them for prototyping
  ahoh <- system(paste0("printf \"%s\n\" `head -n 1 ", file_fyc, " | sed -e 's/||/ /g' | sed -e 's/|//g' | sed -e 's/I 0/I0/g' | sed -e 's/C 0/C0/g' | \
                        sed -e 's/\\([A-Z]\\) OME/\\1_OME/g' | sed -e 's/\\([A-Z]\\) PHI/\\1_PHI/g' | sed -e 's/\\([A-Z]\\) PSI/\\1_PSI/g' | \
                        sed -e 's/\\([A-Z]\\) CHI/\\1_CHI/g' | \
                        sed -e 's/\\([A-Z]\\) NUC/\\1_NUC/g'` | awk -v rs=0 '{if ((NR > 1) && (length($1) > 5)) {rs = rs + 1; split($1, resname, \"_\"); \
                        printf(\"%5d : %10s %5d\\n\", NR, $1, rs)}; if ((NR > 1) && (length($1) <= 5)) {printf(\"%5d : %10s %5d\\n\", NR, resname[1]\"_\"$1, rs);}}'"),
                 intern = TRUE)
  expect_true(!is.null(ahoh))
  
  .ev_it(asd <- CampaRi::get_TBC_resolutions(logfile = file_log))
  .ev_it(asd <- CampaRi::get_TBC_resolutions(logfile = file_fyc), err=T)
  
  .ev_it(asd <- CampaRi::get_TBC_centroids(logfile = file_log, th = 2))
  expect_equal(asd$center[1], 4233)
  expect_warning(CampaRi::get_TBC_centroids(logfile = file_log, level=1, th = 2.222)) # both level and th provided
  .ev_it(asd <- CampaRi::get_TBC_centroids(logfile = file_log, th = 5.0), err=T) # th not present
  .ev_it(asd <- CampaRi::get_TBC_centroids(logfile = file_log, th = 2.222), err=T) # level corresponding to th is not refined 
  .ev_it(asd <- CampaRi::get_TBC_centroids(logfile = file_log, level = 5), err=T) # level not refined
  .ev_it(asd <- CampaRi::get_TBC_centroids(logfile = file_log, level = 50), err=T) # level out of bounds
  
})
