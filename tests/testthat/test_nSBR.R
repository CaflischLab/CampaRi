context("nSBR")

test_that("new trials for SBR", {

  # ------------------- =
  # setting up the test
  silent <- T
  plt_stff <- !silent
  withr::local_package("tictoc")
  withr::defer(.loop_file_rm(grep(x = list.files(), pattern = "to_d", value = T)))
  withr::defer(graphics.off())
  
  # ------------------- =

  stdd <- 3
  n_dim <- 10
  n_snap <- 3000
  n_tot <- n_dim * n_snap / 3
  if (!silent) print(n_tot)
  ann <- c(rep(1, n_snap / 6), rep(2, n_snap / 3), rep(1, n_snap / 6), rep(3, n_snap / 3))
  file.pi <- system.file("extdata", "REPIX_000000000021.dat", package = "CampaRi")
  if (plt_stff) sapphire_plot(sap_file = file.pi, ann_trace = ann, only_timeline = F, timeline = T)
  data.pi <- withr::with_package("data.table", fread(file = file.pi, data.table = F))
  nr <- floor(nrow(data.pi) / 2)
  tot_lpi <- nrow(data.pi)
  data.pi <- rbind(data.pi[(nr + 1):tot_lpi, ], data.pi[1:nr, ])
  data.pi[, 1] <- 1:tot_lpi

  # BASINS OPTIMIZATION ======================================================================================
  nbin <- round(sqrt(n_snap * 10))
  if (!silent) print(nbin)
  .ev_it(optimal_bas <- nSBR(data = file.pi, ny = 30, n.cluster = 4, plot = T, silent = silent, dbg_nSBR = F), s = silent, err = F)


  # errors -------------------------------------------------------
  .ev_it(a1 <- nSBR(
    data = file.pi, n.cluster = 3,
    comb_met = c("MIC_kin"),
    unif.splits = seq(5, 100, 8),
    pk_span = 500, ny = 50, plot = T, random_picks = 100,
    silent = silent, dbg_nSBR = F, return_plot = F
  ), s = silent, err = T)

  # shuffles test -------------------------------------------------------
  .ev_it(a1 <- nSBR(
    data = file.pi, n.cluster = 3, shuffles = T,
    comb_met = "MIC",
    unif.splits = seq(5, 100, 8),
    pk_span = 500, ny = 20, plot = T, random_picks = 100, ann = ann,
    silent = silent, dbg_score_sapphire = F, return_plot = F
  ), silent)
  # .ev_it(a1 <- nSBR(data = file.pi, n.cluster = 3, shuffles = T,
  #                                  comb_met = c('MIC'),
  #                                  unif.splits = seq(5, 100, 8),
  #                                  pk_span = 500, ny = 20, plot = T, random_picks = 100, ann = ann,
  #                                  silent = F, return_plot = F), s = silent)

  # force_correct_ncl test -------------------------------------------------------
  # In this case the number of clusters are forced to be n.clusters.
  .ev_it(a1 <- nSBR(
    data = file.pi, n.cluster = NULL, force_correct_ncl = T, ny = 10, pk_span = 500,
    unif.splits = seq(5, 100, 8), silent = silent, plot = T
  ), s = silent, err = T) # collision with the force number of clusters
  .ev_it(a1 <- nSBR(
    data = file.pi, n.cluster = 8, force_correct_ncl = T, ny = 100, pk_span = 350,
    unif.splits = seq(5, 100, 8), silent = silent, plot = T
  ), s = silent)
  .ev_it(a1 <- nSBR(
    data = file.pi, n.cluster = 4, force_correct_ncl = T, ny = 100, pk_span = 350,
    unif.splits = seq(5, 100, 8), silent = silent, plot = T
  ), s = silent)
  .ev_it(a1 <- nSBR(
    data = file.pi, n.cluster = 5, force_correct_ncl = T, ny = 100, pk_span = 350,
    unif.splits = seq(5, 100, 8), silent = silent, plot = T
  ), s = silent)
  # the flag force_correct_ncl makes it fail if the number of cluster is not met
  .ev_it(a1 <- nSBR(
    data = file.pi, n.cluster = 125, force_correct_ncl = T, ny = 10, pk_span = 500, over_mic_mean = T,
    unif.splits = seq(5, 100, 8), silent = silent, plot = T
  ), s = silent, err = T) # too many clusters in comparison to the one you can find using the peak finding
  .ev_it(a1 <- nSBR(
    data = file.pi, n.cluster = 25, force_correct_ncl = F, ny = 10, pk_span = 500, over_mic_mean = F,
    max_dec_loops = 70, # this is the number of loops for the 10%-decrease of span
    print_span_convergence = T, # this is a supposedly internal variable to plot the decreasing of the span (or increasing) and its causes
    unif.splits = seq(5, 100, 8), silent = silent, plot = T
  ), s = silent, err = F) # This will warn you about the lack of it

  # plotting histograms -------------------------------------------------------
  # .ev_it(a1 <- nSBR(data = file.pi, n.cluster = 3, comb_met = c('MEV'), unif.splits = seq(5, 100, 8), pk_span = 500, ny = 20, plot = T,
  #                                  silent = silent, dbg_nSBR = F), s = silent, err = T) # must use MIC
  # .ev_it(a1 <- nSBR(data = file.pi, n.cluster = 3, comb_met = c('MIC'), unif.splits = seq(5, 100, 8), pk_span = 500, ny = 20, plot = T,
  #                                  silent = silent, dbg_nSBR = F, return_plot = T, hist_exploration = T), s = silent)

  # in parallel! -------------------------------------------------------
  # .ev_it(a1 <- nSBR(data = file.pi, n.cluster = 3, comb_met = c('MIC'), unif.splits = seq(5, 100, 8), pk_span = 200, ny = 30, plot = T,
  #                           silent = silent, dbg_nSBR = F, n_cores = 10, return_plot = T), s = silent, err = T) # too many cores # often does not work
  .ev_it(a1 <- nSBR(
    data = file.pi, n.cluster = 3, comb_met = c("MIC"), unif.splits = seq(5, 100, 8), pk_span = 200, ny = 30, plot = T,
    silent = silent, dbg_nSBR = F, n_cores = 1, return_plot = T
  ), s = silent)

  # speed tests # not the best not worth it
  # mcb <- microbenchmark::microbenchmark(
  #   a1 = nSBR(data = file.pi, n.cluster = 3, comb_met = c('MIC'), unif.splits = seq(5, 100, 8), pk_span = 200, ny = 50, plot = T,
  #                       silent = F, dbg_nSBR = F, n_cores = 8, return_plot = T),
  #   a2 = nSBR(data = file.pi, n.cluster = 3, comb_met = c('MIC'), unif.splits = seq(5, 100, 8), pk_span = 200, ny = 50, plot = T,
  #                            silent = F, dbg_nSBR = F, return_plot = T),
  #   times = 10
  # )
  # autoplot(mcb)


  # testing automatic span increaser -------------------------------------------------------
  .ev_it(a1 <- nSBR(
    data = file.pi, n.cluster = 5, comb_met = c("MIC"), max_dec_loops = 1, force_correct_ncl = T,
    unif.splits = seq(5, 100, 8), pk_span = 1496, ny = 20, plot = T, ann = ann, silent = silent
  ), s = silent, err = T) # number of clusters not possible to find
  .ev_it(a1 <- nSBR(
    data = file.pi, n.cluster = 2, comb_met = c("MIC"), max_dec_loops = 0,
    unif.splits = seq(5, 100, 8), pk_span = 1496, ny = 20, plot = T, ann = ann, silent = silent
  ), s = silent, err = T) # max_dec_loops too low
  .ev_it(a1 <- nSBR(
    data = file.pi, n.cluster = 2, comb_met = c("MIC"), max_dec_loops = "mele",
    unif.splits = seq(5, 100, 8), pk_span = 1496, ny = 20, plot = T, ann = ann, silent = silent
  ), s = silent, err = T) # max_dec_loops not a single int
  .ev_it(a1 <- nSBR(
    data = file.pi, n.cluster = 2, comb_met = c("MIC"), unif.splits = seq(5, 100, 8),
    pk_span = 1496, ny = 20, plot = T, ann = ann, silent = silent, max_dec_loops = 50
  ), s = silent)

  # CVI -----------------------------------------------------------------------------------------------------------------------------------------
  # to show: the cvi index plot
  .ev_it(nSBR(data = file.pi, comb_met = c("euclidean"), unif.splits = seq(5, 120, 8), pk_span = 200, ny = 50, plot = T, use_density = F, optimal_merging = T), s = silent) # use density = F (std is T)
  .ev_it(nSBR(data = file.pi, comb_met = c("euclidean"), unif.splits = seq(5, 120, 8), pk_span = 200, ny = 50, plot = T, use_density = T, optimal_merging = T, ), s = silent) # use density = T (std is T)

  # using the optim_max_ncl_range for optimal_merging which sets the range of possible number of cluster to optimize along with clu_val_ind_met which sets the cluster validity index method
  .ev_it(nSBR(data = file.pi, comb_met = c("euclidean"), unif.splits = seq(5, 120, 8), pk_span = 200, ny = 50, plot = T, optimal_merging = T, optim_max_ncl_range = "all", clu_val_ind_met = "Ball_Hall"), s = silent)
  .ev_it(a <- nSBR(data = file.pi, comb_met = c("euclidean"), unif.splits = seq(5, 120, 8), pk_span = 200, ny = 50, plot = T, optimal_merging = T, optim_max_ncl_range = "less_than_all", clu_val_ind_met = "PBM", return_plot = T), s = silent)
  .ev_it(b <- nSBR(data = file.pi, comb_met = c("euclidean"), unif.splits = seq(5, 120, 8), pk_span = 200, ny = 50, plot = T, optimal_merging = T, optim_max_ncl_range = "less_than_half_all", clu_val_ind_met = "PBM", return_plot = T), s = silent)
  # ggsave(a$plt_list[[3]] + ylab("Euclidean distance"), filename = "barriers_example_wrong.png", width = 5, height = 2.6, path = "~/projects/FSST/thesis_plots/")
  # ggsave(b$plt_list[[3]] + ylab("Euclidean distance"), filename = "barriers_example_right.png", width = 5, height = 2.6, path = "~/projects/FSST/thesis_plots/")
  
# Sets the number of clusters at the end of the optimal_merging. The variables mean_minusSdest_cl_first allows the labelling to follow mean(cl) - sd(cl) policy
  .ev_it(nSBR(data = file.pi, comb_met = c("euclidean"), unif.splits = seq(5, 120, 8), pk_span = 200, ny = 50, plot = T, optimal_merging = T, cvi_ncl = 10, meanMinusSdest_cl_first = T), s = silent) # fixing the number of cvi_ncl
  .ev_it(nSBR(data = file.pi, comb_met = c("euclidean"), unif.splits = seq(5, 120, 8), pk_span = 200, ny = 50, plot = T, optimal_merging = T, cvi_ncl = 10, meanest_cl_first = T), s = silent) # mean value first
  .ev_it(nSBR(data = file.pi, comb_met = c("euclidean"), unif.splits = seq(5, 120, 3), pk_span = 200, ny = 150, plot = T, optimal_merging = T, cvi_ncl = 4, biggest_cl_first = T), s = silent) # largest cluster first
  .ev_it(nSBR(data = file.pi, comb_met = c("euclidean"), unif.splits = seq(5, 120, 3), pk_span = 200, ny = 150, plot = T, use_density = F, optimal_merging = T, cvi_ncl = 4, biggest_cl_first = T), s = silent) # density = F

  # ny/us exploration --------------------------------------------------------------------------------------------------------------------------
  .ev_it(explo_bar_par(pi_f = file.pi, n_snaps = n_snap, ann = ann, peak_span = 10, do_par = T, plt_basename = "to_d", selection_met = "max_cvi", silent = F), s = silent)
  .ev_it(explo_bar_par(pi_f = file.pi, n_snaps = n_snap, ann = ann, peak_span = 10, do_par = F, plt_basename = "to_d", selection_met = "top_5_cvi", silent = F), s = silent)
  .ev_it(asd <- explo_bar_par(pi_f = file.pi, n_snaps = n_snap, ann = ann, peak_span = 10, do_par = F, plt_basename = "to_d", selection_met = "cvi_bins", silent = F, step_us = 3), s = silent)

  # using diff clustering -----------------------------------------------------------------------------------------------------------------------
  .ev_it(nSBR(data = file.pi, comb_met = c("euclidean"), unif.splits = seq(5, 120, 5), pk_span = 20, ny = 150, plot = T, return_plot = T, optimal_merging = T, dis_clu_met = "mds_clara"))
  .ev_it(nSBR(data = file.pi, comb_met = c("euclidean"), unif.splits = seq(5, 120, 5), pk_span = 20, ny = 150, plot = T, return_plot = T, optimal_merging = T, dis_clu_met = "mds_kmeans"))
  .ev_it(nSBR(data = file.pi, comb_met = c("euclidean"), unif.splits = seq(5, 120, 5), pk_span = 200, ny = 150, plot = T, return_plot = T, optimal_merging = T, dis_clu_met = "hdbscan"))
  .ev_it(nSBR(data = file.pi, comb_met = c("euclidean"), unif.splits = seq(5, 120, 5), pk_span = 200, ny = 150, plot = T, return_plot = T, optimal_merging = T, dis_clu_met = "hc_single"))
  .ev_it(nSBR(data = file.pi, comb_met = c("euclidean"), unif.splits = seq(5, 120, 5), pk_span = 200, ny = 150, plot = T, return_plot = T, optimal_merging = T, dis_clu_met = "hc_average"))

  # kinetic selection -----------------------------------------------------------------------------------------------------------------------------
  .ev_it(nSBR(file.pi, comb_met = "kin", ny = 50, pk_span = 200, unif.splits = seq(5, 120, 8), plot = T, optimal_merging = T), err = T) # this is based only on the kinetic annotation, nothing else allowed
  .ev_it(nSBR(file.pi, comb_met = "kin", ny = 50, pk_span = 200, unif.splits = seq(5, 120, 8), plot = T, optimal_merging = F), err = F)
  .ev_it(nSBR(file.pi, comb_met = "kin", ny = 50, pk_span = 200, unif.splits = seq(5, 120, 8), plot = T, optimal_merging = F, parabolic_subtraction = F))
  if(F){
    # example plot for thesis
    a <- nSBR(data = file.pi, comb_met = c("euclidean"), unif.splits = seq(5, 120, 8), pk_span = 200, ny = 50, plot = T, optimal_merging = T, optim_max_ncl_range = "less_than_all", clu_val_ind_met = "PBM", return_plot = T)
    b <- nSBR(data = file.pi, comb_met = c("euclidean"), unif.splits = seq(5, 120, 8), pk_span = 200, ny = 50, plot = T, optimal_merging = T, optim_max_ncl_range = "less_than_half_all", clu_val_ind_met = "PBM", return_plot = T)
    # ggsave(a$plt_list[[3]] + ylab("Euclidean distance"), filename = "barriers_example_wrong.png", width = 5, height = 2.6, path = "~/projects/FSST/thesis_plots/")
    # ggsave(b$plt_list[[3]] + ylab("Euclidean distance"), filename = "barriers_example_right.png", width = 5, height = 2.6, path = "~/projects/FSST/thesis_plots/")
    
    mi_met <- c('MIC', "kin") # 'MIC_kin', 'kin', 'diff', 'convDiff', 'conv'
    phil_met <- c("euclidean", "manhattan", "cosine", 
                               "pearson", "kullback-leibler", "jensen-shannon")
    it_met <- "euclidean"
    library(parallel); library(tictoc);library(CampaRi); library(data.table)
    source("/home/dgarolini/projects/CampaR/CampaRi/R/misc.R")
    cat2 <- function(...){
      system(paste("echo '",...,"'"))
    }
    fin_asd <- NULL
    plots <- F
    
    for(fo in c(0.1, 0.5)){
      for(i in 1:100){
        file.pi <- "PItest_todel.dat"
        while(T){
          trj_tmp <- tryCatch(CampaRi::run_MSM_simulation(
            n_snaps = 3000, n_states = 5, n_feats = 9, n_noise_vars = 1, inside_noise_perc = 0.01,
            kernel_met = "norm", feat_distr_m = "overlap", plotit = F, feat_overlap = fo), error = function(e) return(e))
          if(!inherits(trj_tmp, "error")) break
        }
        campari_SST(trj = trj_tmp$trjdat, sap_file = file.pi, search.attempts = 3000, silent = T)
        if(plots) sapphire_plot(sap_file = file.pi, ann_trace = trj_tmp$ann, timeline = T, timeline_proportion = 0.5)
        asd <- lapply(c(mi_met, phil_met), FUN = function(it_met){
          tic()
          # cat2(it_met, "\n")
          a <- nSBR(data = file.pi, comb_met = it_met, unif.splits = seq(5, 120, 8), 
                    pk_span = 200, ny = 50, plot = plots, optimal_merging = F, return_plot = F, over_mic_mean = T)
          ar <- score_sapphire(file.pi, ann = trj_tmp$ann, manual_barriers = a$bar_over_mean, plot_pred_true_resume = plots)
          return(list("t" = .toc_s(), "n_cl" = .ltuni(a$df_centers$clu_ind), "ARI" = ar$score.out, "met" = it_met, "fo" = fo))
        })#, mc.cores = 8)
        fin_asd <- c(fin_asd, asd)
      }
    }
    fut_tut <- bind_rows(fin_asd) %>% arrange(desc(ARI)) %>% 
      # dplyr::filter(!met %in% c("squared_euclidean", "inner_product", "harmonic_mean")) 
      dplyr::filter(!met %in% c("kin", "squared_euclidean", "inner_product", "harmonic_mean", "chebyshev")) 
    fututot <- fut_tut %>% group_by(met) %>% summarise(m = mean(ARI))
    agata <- fut_tut %>%
      ggplot() + geom_violin(aes(x = as.factor(met), y = ARI)) +
                             # , col = as.factor(fo))) +
      geom_point(data = fututot, aes(x = as.factor(met), y = m)) +
      theme(axis.text.x = element_text(angle = 45, hjust = 1, vjust = 1)) +
      xlab("Method")
    agata
    ggsave(plot = agata, filename = "for_thesis_methods_divisions.png", width = 3.5, height = 4, path = "~/projects/FSST/thesis_plots/")
  }
})
