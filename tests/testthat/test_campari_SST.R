context('campari_SST')

test_that('tests for campari_SST', {
  
  
  # ------------------- =
  # setting up the test
  silent <- T
  plt_stff <- !silent
  withr::local_package("tictoc")
  withr::defer(.loop_file_rm(
    c(
      "basename.tsv",
      "basename.log",
      "sap_file",
      "PI_output_basename_pistart1.dat",
      list.files("STRUCT_CLUSTERING"),
      list.files("Rplots")
    )
  ))
  # ------------------- =
  
  # find_perc -------------------------------------------------------------------------------------------------------------------------------
  trj <- as.data.frame(matrix(rnorm(n = 100000, mean = 5, sd = 2), nrow = 1000, ncol = 100))
  var_weights <- sample(c(0.2,0.4,0.9), 100, replace = T)
  .ev_it(find_perc_TBC(trj = trj, n_sampling = 100, silent = silent, perc = c(0.2, 0.6), n_cores = 1000), s = silent, err = T)
  .ev_it(find_perc_TBC(trj = trj, n_sampling = 100, silent = silent, perc = c(0.2, 0.6, 0.9)), s = silent, err = T)
  .ev_it(find_perc_TBC(trj = trj, n_sampling = 100, silent = silent, perc = c(0.2, 0.7), plot = T, n_cores = 1), s = silent)
  .ev_it(find_perc_TBC(trj = trj, n_sampling = 500, var_weights = var_weights[-1], silent = silent, perc = c(0.2, 0.7), plot = T, n_cores = 2), s = silent, err = T)
  .ev_it(find_perc_TBC(trj = trj, n_sampling = 500, var_weights = var_weights, silent = silent, perc = c(0.2, 0.7), plot = T, n_cores = 1), s = silent)
  
  
  
  # campari_SST -------------------------------------------------------------------------------------------------------------------------------
  main_dir <- system.file('extdata/for_campari/', package = "CampaRi")
  bin_dir <- paste0(main_dir, '/', dir(main_dir)[1], '/bin/')
  ca_exe <- paste0(bin_dir, '/', dir(bin_dir)[1])
  .ev_it(campari_SST(trj, camp_exe = ca_exe, base_name = "basename", sap_file = "sap_file", silent = silent, multi_threading = F, rm_extra_files = T, 
                    cmax_rad = NULL, cradius = NULL, pcs = NULL, state.width = NULL, cdistance = NULL, pi.start = NULL, search.attempts = NULL, birchheight = NULL,
                    leaves.tofold = NULL), s = silent)
  .ev_it(campari_SST(trj, camp_exe = ca_exe, base_name = "basename", sap_file = NULL, silent = silent, multi_threading = F, rm_extra_files = T, 
                    cmax_rad = NULL, cradius = NULL, pcs = NULL, state.width = NULL, cdistance = NULL, pi.start = NULL, search.attempts = NULL, birchheight = NULL,
                    leaves.tofold = NULL), s = silent)
  .ev_it(campari_SST(trj, camp_exe = ca_exe, base_name = "basename", sap_file = NULL, silent = silent, multi_threading = F, rm_extra_files = T, 
                    cmax_rad = NULL, cradius = NULL, pcs = 3, state.width = NULL, cdistance = NULL, pi.start = NULL, search.attempts = NULL, birchheight = NULL,
                    leaves.tofold = NULL), s = silent)
  # MST
  # .ev_it(campari_SST(trj, camp_exe = ca_exe, base_name = "basename", sap_file = NULL, silent = silent, multi_threading = F, rm_extra_files = T, MST = T,
  #                   cmax_rad = NULL, cradius = NULL, pcs = 3, state.width = NULL, cdistance = NULL, pi.start = NULL, search.attempts = NULL, birchheight = NULL,
  #                   leaves.tofold = NULL), s = silent)

})




