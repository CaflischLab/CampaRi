context('generate_network')

test_that('pre-processing with network inference', {
  # ------------------- =
  # setting up the test
  silent <- T
  plt_stff <- !silent
  withr::local_package("tictoc")
  # ------------------- =
  
  test_plotting <- F
  my_libs <- F
  if(my_libs) withr::local_package("TSA")

  trj <- as.data.frame(matrix(rnorm(1000), nrow = 100, ncol = 10))
  expect_true(!is.null(trj))
  
  # errors -----------------------------------
  .ev_it(a <- generate_network(trj, window = 20, method = "sadasd", silent = silent), err = T)
  .ev_it(a <- generate_network(trj, window = 20, pp_method = "asdsads", silent = silent), err = T)
  
  # difftesting for distances and postprocessing ----------------------------------
  .ev_it(net_joint_tr <- generate_network(trj, method = 'minkowski', pp_method = 'svd', window = 12, silent = silent), NA)
  .ev_it(net1 <- generate_network(trj, method = 'minkowski', window = 20, silent = silent), NA)
  # .ev_it(path_net <- generate_network(trj, method = 'minkowski', pp_method = 'path_minkowski', window = 15, silent = silent), NA) # PairViz diff to reinstall
  .ev_it(SVD_net <- generate_network(trj, method = 'minkowski', pp_method = 'svd', window = 20, silent = silent), NA)
  # expect_error(MI_net <- generate_network(trj, method = 'MI', pp_method = "SymmetricUncertainty", window = 7), NA)
  
  # tsne -----------------------------------
  # expect_error(tsne_net <- generate_network(trj, method = 'minkowski', pp_method = 'tsne', window = 20, tsne_pearagd = 3, silent = silent))
  # expect_error(tsne_net <- generate_network(trj, method = 'minkowski', pp_method = 'tsne', window = 20, tsne_perplexity = 3), NA)
  .ev_it(tsne_net <- generate_network(trj, method = 'none', pp_method = 'tsne', window = 20, silent = silent, use_decreasing_borders = F), NA)
  
  # multiplication -----------------------------------
  .ev_it(a <- generate_network(trj, window = 20, method = "sadasd", silent = silent), err = T)
  .ev_it(a <- generate_network(trj, window = 20, method = "multiplication", multiplication_points = c(-10,9), silent = silent), err = T)
  .ev_it(a <- generate_network(trj, window = 20, method = "multiplication", multiplication_points = c(-8,11), silent = silent), err = T)
  .ev_it(a <- generate_network(trj, window = 20, method = "multiplication", multiplication_points = c(-8,8), silent = silent))
  .ev_it(a <- generate_network(trj, window = 20, method = "multiplication", multiplication_points = c(-8,8), silent = silent, use_decreasing_borders = F))
  
  # spectrum transf -----------------------------------
  trj <- as.data.frame(matrix(NA, nrow = 120, ncol = 2))
  trj[,1] <- sin(1:120)*(1/rep(1:30, 4) + tan(120:1)*(1/100))
  trj[,2] <- cos(1:120)*(1/rep(1:30, 4) + tanh(120:1)*(1/sin(120:1)))
  if(test_plotting) plot(trj[,1], type = 'l')
  if(test_plotting) plot(trj[,2], type = 'l')
  if(my_libs) a <- periodogram(y = trj[ ,1], plot = test_plotting)
  if(my_libs) b <- periodogram(y = trj[ ,2], plot = test_plotting)
  if(test_plotting) plot(y = b$spec, x = b$freq,type = "h") # spec is what it was needed

  .ev_it(asd <- generate_network(trj, method = 'fft', window = 10, silent = silent), NA)
  if(my_libs) c <- periodogram(y = c(trj[1:5, 1], trj[1:5, 1]), plot = test_plotting)
  if(my_libs) d <- periodogram(y = c(trj[1:5, 2], trj[1:5, 2]), plot = test_plotting)
  # final check of equality
  if(my_libs) expect_true(all(c$spec == asd[1, 1:5]))
  if(my_libs) expect_true(all(d$spec == asd[1, 6:10]))
  
  if(test_plotting) plot(y = c$spec, x = c$freq, type = 'h')
  if(test_plotting) plot(y = asd[1, 1:5], x = c$freq, type = 'h')
  if(test_plotting) plot(y = d$spec, x = d$freq, type = 'h')
  if(test_plotting) plot(y = asd[1, 6:10], x = c$freq, type = 'h')
  
  # more staff -> range & freq
  # amplitude (range)
  # expect_warning(asd <- generate_network(trj, pp_method = "amplitude", window = 10, silent = silent))
  .ev_it(asd <- generate_network(trj, pp_method = "amplitude", window = 10, silent = silent), NA)
  res <- c()
  for(i in 1:ncol(trj))
    res <- c(res, diff(range(c(trj[1:5, i], trj[1:5, i]))))
  expect_true(all(res == asd$trj_out[1, ]))
  # maxfreq
  # expect_warning(asd <- generate_network(trj, pp_method = "maxfreq", window = 10, silent = silent))
  .ev_it(asd <- generate_network(trj, pp_method = "maxfreq", window = 10, silent = silent), NA)
  res <- c()
  if(my_libs){
    for(i in 1:ncol(trj)){
      P <- periodogram(c(trj[1:5, i], trj[1:5, i]), plot = F)
      res <- c(res, P$freq[which(P$spec == max(P$spec))])
    }
    expect_true(all(res == asd[1, ]))
  }
  # both
  # expect_warning(asd <- generate_network(trj, pp_method = "amplitude_maxfreq", window = 10, silent = silent))
  .ev_it(asd <- generate_network(trj, pp_method = "amplitude_maxfreq", window = 10, silent = silent), NA)
  
  # MIC -----------------------------------
  .ev_it(b <- generate_network(trj = trj, window = 10, method = 'MIC', silent = silent), NA) # multithreading MIC is super bad
  
  # svd -----------------------------------
  asd <- generate_network(trj, method = "euclidean", pp_method = "svd", window = 10, silent = silent)
  
  # EMD ------------------------------------
  # pre-selection fastest method; typical application is a 50X100 mat
  .ev_it(asd <- generate_network(trj, method = "sigemd", window = 10, silent = silent))
  # .ev_it(asd <- generate_network(trj, method = "wgcna_emdl1", window = 10, silent = silent))
  # .ev_it(asd <- generate_network(trj, method = "emdl1", window = 10, silent = silent))
  
  # PARALLEL par_generate_network =========================================================================================================
  data_f <- matrix(rnorm(100000), nrow =  10000, ncol = 5)
  .ev_it(parresnet <- par_generate_network(data_f, n_cores = 1, window = 100), err = T)
  .ev_it(parresnet <- par_generate_network(data_f, n_cores = 2, window = 100), err = F)
  .ev_it(resnet <- generate_network(data_f, window = 100, silent = silent)$trj_out, NA)
  # expect_equal(resnet, parresnet) # to be checked better!!! what did change?
  .ev_it(parresnet <- par_generate_network(data_f, n_cores = 1002, window = 100), err = T) # error too many cores
  .ev_it(parresnet <- par_generate_network(12, n_cores = 1, window = 100), err = T) # error wrong trj insertion
  # ALL MET & OPTIMIZATION =========================================================================================================
  # data_f <- .rnorm.mat(15000, 5)
  # library(tictoc)
  # silent <- F
  # profvis::profvis(resnet <- CampaRi::generate_network(data_f, window = 100, silent = silent, method = "euclidean", dbg_gn = F))
  # avail_methods <- c(                                                                                   # only post-proc
  #                    'wgcna',                                                                                  # cor
  #                    'binary', 'euclidean', 'manhattan', 'maximum', 'canberra', 'minkowski', #'mahalanobis',   # distances
  #                    'covariance',                                                                             # cov
  #                    'MI', 'MI_MM', 'MI_ML', 'MI_shrink', 'MI_SG',                                             # MI based
  #                    'MIC', 'MAS', 'MEV', 'MCN', 'MICR2', 'GMIC', 'TIC',                                       # mine based measures (minerva)
  #                    'fft',                                                                                    # fft 
  #                    # "wgcna_emdl1",
  #                    # "emdl1", 
  #                    "sigemd",
  #                    "multiplication")     
  # avail_post_proc_met <- c('path_euclidean', 'path_minkowski', 'path_manhattan', 'path_maximum', 'path_canberra', 'path_binary', # find_path
  #                          'svd', 
  #                          'tsne',
  #                          'SymmetricUncertainty',
  #                          'amplitude', 'amplitude_maxfreq', 'maxfreq')
  # ot <- tibble("met" = avail_methods, "time" = NA, "n_na" = NA)
  # for (i in avail_methods) {
  #   cat("Doing", i, "\n")
  #   tic()
  #   resnet <- generate_network(data_f, window = 500, silent = silent, method = i, dbg_gn = F, na_thresh = 100)
  #   ot <- ot %>% mutate(time = replace(time, met == i, .toc_s())) %>% 
  #     mutate(n_na = replace(n_na, met == i, resnet$n_na_trj))
  # }
  
  # ot_fin <- ot %>% add_column("l" = 5000)
  # ot_fin2 <- ot %>% add_column("l" = 15000)
  # fin_out_fin <- bind_rows(ot_fin, ot_fin2)
  # fof <- fin_out_fin %>% mutate("proj" = time/l*130000)
  # save(fof, file = "fof.Rdata")
  # load("fof.Rdata")
  # fof %>% arrange(desc(time)) %>% mutate("normalized" = time / l) 
  # as I am using mainly the euclidean metrics lets see if the WGCNA wrapping is indeed slowing down the process
  # tmp_trj <- data_f 
  # res <- microbenchmark::microbenchmark("wgcna" = WGCNA::adjacency(tmp_trj, type = "distance", distOptions = 'method = "euclidean"'),
  #                                       "direct" = as.matrix(dist(tmp_trj)),
  #                                       "multivars" = as.matrix(multivariance::fastdist(tmp_trj)), times = 100)
  # 
  # ggplot2::autoplot(res) # the winner is WGCNA!!!!
  # res
  # XCORR SPIKE TIMES =========================================================================================================
  # data_f <- matrix(rnorm(100000), nrow =  1000, ncol = 30)
  # profvis::profvis(resnet <- CampaRi::gen_net_from_spti(data_f, window = 100, silent = silent, method = "euclidean", dbg_gn = F))
  
})
