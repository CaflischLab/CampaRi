
[![pipeline status](https://gitlab.com/CaflischLab/CampaRi/badges/master/pipeline.svg)](https://gitlab.com/CaflischLab/CampaRi/commits/master)
[![coverage report](https://gitlab.com/CaflischLab/CampaRi/badges/master/coverage.svg)](https://codecov.io/gl/CaflischLab/CampaRi)
[![DOI](https://zenodo.org/badge/68593949.svg)](https://zenodo.org/badge/latestdoi/68593949)


------------------------------------------------------------------------
# CampaRi - An R package for time-series analysis

CampaRi is a package for the analysis of time-series. Initially conceived as an R wrapper of the original [campari](http://campari.sourceforge.net/documentation.html) software package, it has been subsequently extended with other techniques. \
Most of the methods here implemented rely on the so-called Progress Index algorithm and SAPPHIRE (States And Pathways Projected with HIgh REsolution) plot; among them, network inference pre-processing (NetSAP), SAPPHIRE-based clustering (SbC). \
See [References](#citation-and-references) for detailed information on the methods. \
Please use the tutorials, vignettes or the function documentation to explore all of the possibilities.

The documentation of the package is available online on [CampaRi doc](https://caflischlab.gitlab.io/CampaRi)

For any help with the package usage and installation please [chat with us](https://gitter.im/CampaRi_chat/Lobby).

------------------------------------------------------------------------
### Installation ###

* Stable version: we will upload the package on CRAN as soon as possible.

* Development version: use `devtools::install_gitlab("CaflischLab/CampaRi", dependencies = "Depends")` to install the package directly from the repository, along with its fundamental dependencies 

For further details on the installation procedure we suggest to read the [installation tutorial](https://caflischlab.gitlab.io/CampaRi/articles/installation.html).

------------------------------------------------------------------------
### Help and Tutorials ###

We are currently writing how-to-use guides with examples and datasets. 
Tutorial and vignettes can be also accessed on [this website](https://caflischlab.gitlab.io/CampaRi).

------------------------------------------------------------------------
### Extensive Manual ###

For full information on the Progress Index algorithm use, please refer to the main documentation of the original [CAMPARI documentation](http://campari.sourceforge.net/documentation.html).
Some good starting points to understand and utilize campari syntax are the [tutorials 1, 10 and 11](http://campari.sourceforge.net/V3/tutorials.html).

------------------------------------------------------------------------
## Citations and references

Please cite the software as

* Garolini, D., Cocina, F. & Langini, C. CampaRi: an R package for time series analysis. (2019). doi:10.5281/zenodo.3428933

For the Progress Index algorithm and SAPPHIRE plot cite:

* Blöchliger, N., Vitalis, A., & Caflisch, A. (2013). A scalable algorithm to order and annotate continuous observations reveals the metastable states visited by dynamical systems. Comput. Phys. Commun. 184, 2446–2453. http://dx.doi.org/10.1016/j.cpc.2013.06.009
* Blöchliger, N., Vitalis, A., & Caflisch, A. (2014). High-resolution visualisation of the states and pathways sampled in molecular dynamics simulations. Sci. Rep. 4, 6264. https://doi.org/10.1038/srep06264

If using NetSAP method:

* Garolini, D., Vitalis, A., & Caflisch, A. (2019). Unsupervised identification of states from voltage recordings of neural networks. J. Neurosci. Methods, 318, 104–117. https://doi.org/10.1016/j.jneumeth.2019.01.019

Eventually, if using <span style="font-variant:small-caps;">SAPPHIRE</span>-based clustering:

* Cocina, F., Vitalis, A., & Caflisch, A. (2020). <span style="font-variant:small-caps;">SAPPHIRE</span>-Based Clustering. J. Chem. Theory Comput., 16, 10. https://doi.org/10.1021/acs.jctc.0c00604
