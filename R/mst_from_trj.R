#' @title Create the minimum spanning tree from time series
#' @description \code{mst_from_trj} creates a minimum spanning tree from a time series (e.g. a trajectory in molecular dynamics) using different distance metrics
#' between pairwise snapshots.
#'
#' @param trj Input trajectory (variables on the columns and equal-time spaced snpashots on the row). It must be a \code{matrix} or a \code{data.frame} of numeric.
#' @param dump_to_netcdf If \code{FALSE} the netcdf support will be used. The minimum spanning tree will be dumped to file for further analysis.
#' @param return_tree_in_r Should we return the minimal tree construction for the MST? As it is produced from the progress index algorithm.
#' @param mode It takes a string in input and can be either "fortran" (highly advised and default) or "R".
#' @param distance_method Distance metric between snapshots. This value can be set 1 (dihedral angles) or 5 (root mean square deviation) or 11 (balistic distance)
#' or 12 (mahalanobis distance - matrix to be inserted).
#' @param clu_radius This numeric argument is used in the clustering step in order to make clusters of the same radius at the base level.
#' @param clu_hardcut This option is used only with \code{birch_clu=F} and defines the inter-clusters distance threshold.
#' @param normalize_d A logical that indicates whether the distances must be normalized or not. Usually used with averaging.
#' @param cores If \code{mode="R"} a complete adjacency matrix can be created in parallel using multiple cores (anyhow slower than "fortran" mode).
#' @param mute_fortran If \code{mute_fortran=T} the function will silence the fortran code.
#' @param silent If \code{TRUE} everything will be silent.
#' @param ... Various variables in addition (metric_mat - mahalanobis distance matrix to preprocess the trj.)
#'
#' @details For more details, please refer to the main documentation of the original campari software \url{http://campari.sourceforge.net/documentation.html}.
#'
#' @return If no netcdf support is available the function will return a list with 3 arguments: node degrees, adjacency list and associated distances.
#' If netcdf support is activated the function will dump the mst in the file "DUMPLING.nc".
#' @seealso \code{\link{gen_progindex}}, \code{\link{gen_annotation}}.
#' @examples
#' adjl <- mst_from_trj(trj = matrix(rnorm(1000), nrow = 100, ncol = 10))
#'
#' @export mst_from_trj
#' @importFrom parallel detectCores stopCluster
#' @useDynLib CampaRi, .registration = TRUE



mst_from_trj <- function(trj,
                         dump_to_netcdf = FALSE,
                         return_tree_in_r = TRUE,
                         mode = "fortran",
                         distance_method = 5,
                         clu_radius = NULL,
                         clu_hardcut = NULL,
                         #inputs
                         normalize_d = FALSE,
                         cores = NULL,
                         mute_fortran = FALSE,
                         silent = FALSE,
                         ...) {
    
  # checking the method input
  mode <- .checkM(mode, c('R', 'fortran'))
  
  # Checking additional inputs
  input_args <- list(...)
  avail_extra_argoments <- c('metric_mat')
  .checkExtraArgs(args = input_args, avail_args = avail_extra_argoments, silent = silent)
  
  
  # Mahalanobis distance preprocessing
  if(!('metric_mat' %in% names(input_args))) metric_mat <- NULL else metric_mat <- input_args[['metric_mat']]

  # Checking trajectory input
  if(!is.matrix(trj)){
    if(!is.data.frame(trj)) stop('trj input must be a matrix or a data.frame')
    trj <- as.matrix(trj)
  }

  # Checking logicals
  .isSingleLogical(dump_to_netcdf)
  .isSingleLogical(return_tree_in_r)
  .isSingleLogical(mute_fortran)
  .isSingleLogical(silent)
  if(silent) mute_fortran <- TRUE

  # Memory handling
  if(!dump_to_netcdf && !silent) cat("Normal memory handling selected (dump_to_netcdf = FALSE). Without hdf5/netcdf backend file management it will be difficult for R to handle big data-sets.\n")
  if(dump_to_netcdf && !silent) cat("Selected data support: netcdf data management (dump_to_netcdf = TRUE).\n")

  # Input setting
  n_snaps <- nrow(trj)
  n_xyz <- ncol(trj)

  # Checking distance value
  sup_dist <- c(1, 5, 12)
  if(length(distance_method) != 1) stop('Only one distance is available per time.')
  if(!is.numeric(distance_method) || !(distance_method %in% sup_dist)) stop("The distance inserted is not valid. Check the documentation for precise values.")
  if(distance_method%%1 != 0) stop('distance_method must be an integer.')

  # Checking the Mahalanobis distance insertion
  if(!is.null(metric_mat)){
    if(!is.matrix(metric_mat)){
      if(!is.data.frame(metric_mat)) stop('metric_mat input must be a matrix or a data.frame')
      metric_mat <- as.matrix(metric_mat)
    }
    if(!is.numeric(metric_mat)) stop('metric_mat must be a numerical vector. ')
    if(nrow(metric_mat) != ncol(metric_mat)) stop('metric_mat must be a squared matrix. ')
    if(nrow(metric_mat) != n_xyz) stop('metric_mat must have same rows and columns as the number of features in input (columns of trj).')
    if(!silent) cat('Mahalanobis distance inserted for generic euclidean distance metric use. This method will use the input matrix metric_mat to compute the sapphire pipeline.\n')
    if(distance_method != 12) stop('Please insert the distance_method accordingly to the Mahalanobis metric_mat mode.')
  }else{
    if(distance_method == 12) stop('Please insert a proper metric_mat if you want to use distance_method == 12 which activates the Mahalanobis distance.')
    metric_mat <- diag(1.0, n_xyz, n_xyz) # create Identity matrix
  }


  # -----------------------------------------------------------------------
  # -----------------------------------------------------------------------
  # Normal mode (R).
  #
  # This feature is using only R in parallel to calculate all the paired
  # distances. It is incredible LONGY.
  #
  #


  if(mode == 1){

    # checking the presence of bio3d
    .check_pkg_installation("bio3d")

    n_cores <- parallel::detectCores() - 1
    if(!is.null(cores)&&(cores%%1==0)) n_cores=cores
    else warning("No or wrong entry for number of cores: using all of them -1")

    dim<-attributes(trj)$dim
    if(dim>1000) stop('the computation could be incredibly long. Please use mode = "fortran" option')

    # Initiate cluster
    cl <- makeCluster(n_cores, setup_strategy = "sequential")
    # clusterExport(cl, "trj")
    # cl <- makeCluster(mc <- getOption("cl.cores", 4))
    # clusterExport(cl=cl, varlist=c("text.var", "ntv", "gc.rate", "pos"))
    # clusterEvalQ(cl, library(rms))

    adjl<-c()
    for(i in 1:dim[1]){
      # if(n_cores>1) clusterExport(cl, "i")
      adjl<-c(adjl, parLapply(cl = cl,X = trj[(i+1):dim[1],],fun = function(x){
        bio3d::rmsd(trj[i,],x)
      }))
    }

    parallel::stopCluster(cl)
    if(!silent) warning('No MST made. Please use igraph package and in particular "mst" function in order to continue the analysis')


  # -----------------------------------------------------------------------
  # -----------------------------------------------------------------------
  # Fortran mode.
  #
  # This is the main routine which is using extracted code from campari.
  # Here all the variables specific to the fortran interface are defined.
  #
  #

  }else if(mode == 2){
    # --------------
    # Default vars
    # --------------

    # Thresholds for radius and inter radius values. This is MST leader clustering
    if(is.null(clu_radius) || clu_radius <= 0){
      clu_radius <- 21474364
      if(!silent) warning(paste("clu_radius variable (a priori fixed clustering radius) has not been selected. A standard value of", clu_radius, "will be used."))
    }
    if(is.null(clu_hardcut) || clu_hardcut <= 0){
      clu_hardcut <- 21474364
      if(!silent) warning(paste("clu_hardcut variable (a priori fixed distance threshold between different cluster members) has not been selected. A standard value of", clu_hardcut, "will be used."))
    }
    .isSingleNumeric(clu_radius)
    .isSingleNumeric(clu_hardcut)

    # Logical inputs check
    .isSingleLogical(normalize_d)

    # ------------------------------------------------------------------------------
    # Main functions for internal calling of Fortran code

    output_fin <- list()
    max_d <- as.integer(0)

    # checking the netcdf possibilities
    dump_to_netcdf <- .Fortran('check_netcdf_installation', PACKAGE="CampaRi", wanting_netcdf_backend=as.logical(dump_to_netcdf))
    dump_to_netcdf <- dump_to_netcdf$wanting_netcdf_backend

    dfffo <- n_snaps
    if(!dump_to_netcdf && !return_tree_in_r) stop('Choose one particular memory handling (either R or Netcdf).')
    if(dump_to_netcdf && !return_tree_in_r) dfffo <- 10 # dimensional_flag_for_fixed_out
    if(!dump_to_netcdf && n_snaps > 25000)
        stop("Using more than 25000 snapshots with no memory handling will generate a memory overflow (tested with 16gb RAM). Please set the option dump_to_netcdf as TRUE.")
    
    
    
    # MAIN CALL TO FORTRAN
    adj_deg <- as.integer(rep(0, dfffo))
    adj_ix <- matrix(as.integer(rep(0, dfffo*dfffo)), dfffo, dfffo)
    adj_dis <- array(0.0, dim = c(dfffo, dfffo))
    # setting the input-output silly variables (R-Fortran communication needs) # only with single precision (real(4))
    # attr(adj_dis, "Csingle") <- TRUE
    # attr(metric_mat, 'Csingle') <- TRUE
    #main fortran talker
    output <- .Fortran("generate_neighbour_list", PACKAGE="CampaRi",
                        #input
                        trj_data=trj,
                        n_xyz_in=as.integer(n_xyz),
                        n_snaps_in=as.integer(n_snaps),
                        dfffo=as.integer(dfffo), # dimensional_flag_for_fixed_out
                        clu_radius_in=clu_radius,
                        clu_hardcut_in=clu_hardcut,
                        #output
                        adjl_deg=adj_deg,
                        adjl_ix=adj_ix,
                        adjl_dis=adj_dis,
                        max_degr=as.integer(max_d),
                        #algorithm details
                        dis_method_in=as.integer(distance_method),
                        metric_mat_in=metric_mat,
                        # mst_in=as.logical(min_span_tree), # ???
                        #modes
                        normalize_dis_in=as.logical(normalize_d),
                        return_tree_in_r=as.logical(return_tree_in_r),
                        dump_tree_nc=as.logical(dump_to_netcdf),
                        mute_in=as.logical(mute_fortran))
    
    if(return_tree_in_r){
      # output adjustment
      output_fin[[1]] <- output$adjl_deg
      output_fin[[2]] <- output$adjl_ix[,1:output$max_degr]
      output_fin[[3]] <- output$adjl_dis[,1:output$max_degr]
      invisible(output_fin)
    }
  }
}
