!-------------------------------------------------------------------------------
!
subroutine dir_distance_f(x, nr, nc, diag, met, disc_met, ent_met, nbins, p_in, d_out)

  ! VARIABLES DEFINITIONS
  ! x - matrix
  ! nr - nrows
  ! nc - ncols
  ! diag - 1 if the diag is considered
  ! met - main dist method
  ! disc_met - method for discretization of the data set
  ! ent_met - entropy type (method)
  ! nbins - number of bins for histogram discretization
  ! p_in - p value for Minkowski distance
  ! d_out - output data set

  ! module calls
  ! use mod_gutenberg
  use mod_dis_interface
  use, intrinsic :: iso_c_binding

  implicit none

  procedure (dist_f), pointer :: dist_f_ptr => null ()

  ! variables
  integer, intent(in) :: nr, nc, met, disc_met, ent_met, nbins
  integer, intent(in) :: diag   ! 1 = yes do the diagonal
  integer i, j
  ! integer disc_data(nr*nc)
  ! integer x_disc(nr,nc)
  ! real(8) x_ar(nr*nc)
  ! integer i1, i2, jj
  real(8), intent(in) :: x(nr, nc), p_in
  real(8), intent(inout) :: d_out(nr, nr)
  ! real(8) dev, dist
  ! print *, x(0,:)

  if (met == 1) then
    dist_f_ptr => euclidean_d
  else if (met == 2) then
    dist_f_ptr => euclidean_d_dot
  else if (met == 3) then
    dist_f_ptr => euclidean_d_mag
  else if (met == 4) then
    dist_f_ptr => maximum_d
  else if (met == 5) then
    dist_f_ptr => manhattan_d
  else if (met == 6) then
    dist_f_ptr => canberra_d
  else if (met == 7) then
    dist_f_ptr => binary_d
  ! else if (met == 10) then
  !   dist_f_ptr => minkowski_d_tmp
  end if

  ! do j = 1, nr, 1
  !   do i = j + diag, nr, 1
  !     i1 = i
  !     i2 = j
  !     do jj = 0, nc, 1
  !       dev = (x[i1] - x[i2])
  !       dist += dev * dev
  !       i1 += nr
  !       i2 += nr
  !     end do
  !     d_out(j,i) = sqrt(dist)
  !   end do
  ! end do
  ! write(*,*) "a", x(nr+1,:)
  if(met < 8) then
    do j = 1, nr, 1
      do i = j + diag, nr, 1
        d_out(j,i) = dist_f_ptr(x(i,:), x(j,:), %val(nc))
        d_out(i,j) = d_out(j,i)
        ! call rpr(REAL(d_out(j,i),KIND=4)) ! with casting
        ! if(i < 5 .and. j < 5) print *, i, "-", j, ":", d_out(j,i)
      end do
    end do
  else if(met .eq. 8) then
    do j = 1, nr, 1
      do i = j + diag, nr, 1
        d_out(j,i) = minkowski_d(x(i,:), x(j,:), %val(nc), %val(p_in))
        d_out(i,j) = d_out(j,i)
      end do
    end do
  else if(met .eq. 9) then
    do j = 1, nr, 1
      do i = j + diag, nr, 1
        d_out(j,i) = euclidean_d_for(x(i,:), x(j,:), nc)
        d_out(i,j) = d_out(j,i)
      end do
    end do
  else if(met .eq. 10) then
    ! do i = 1, nc, 1
    !   x_ar(((i-1)*nr + 1):(i*nr)) = x(:,i)
    ! end do
    ! if(disc_met .eq. 1) then
    !   call discEF(x_ar, disc_data, %val(nr), %val(nc), %val(nbins))
    ! end if
    ! do i = 1, nc, 1
    !   print *, disc_data(((i-1)*nr + 1):(i*nr))
    !   x_disc(:,i) = disc_data(((i-1)*nr + 1):(i*nr))
    ! end do
    ! print *, ""
    do j = 1, nr, 1
      do i = j + diag, nr, 1
        ! d_out(j,i) = mutinfo(x(i,:), x(j,:), %val(nc), %val(ent_met), %val(nbins))
        d_out(j,i) = mutinfo(x(i,:), x(j,:), nc, ent_met, nbins)
        d_out(i,j) = d_out(j,i)
      end do
    end do
  end if
end
