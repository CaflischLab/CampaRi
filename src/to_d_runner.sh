#!/bin/bash

gcc -c -g c_distance.c
# gcc -c -g c_info.c
gfortran -c -g mod_dis_interface.f90
gfortran -c -g direct_distance.f90
gfortran -g to_d_distances.f90 c_distance.o mod_dis_interface.o direct_distance.o -o to_d_GO
rm *.o *.mod
./to_d_GO
rm to_d_GO
