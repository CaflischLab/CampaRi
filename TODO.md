# TODO
This file contains a loose collection of objectives for the future aspects of the development. It is subdived by priority level.

## High priority

* Change package name

* Refine and complete the documentations, with citations and example/vignettes that cover the majority of features.

* Reform and complete the `sapphire_plot` function, by accompishing the following steps:
    - let’s focus the development on sapphire_plot_simply and please read the function and modify what you think it is best. I will remove some stuff related to saving the plot, as Francesco and me decided to leave these details to the users (too many shallow variables to push forward for little use).
    - I will add a plot_annotation function very soon. Also here, check if it conforms to your need and style after I pushed it to the repo (today). For the legend problem, please refer to common practice file.
    - Polish the vignette and ditch the old function.

* Take out the save plot options from all functions.

## Medium priority

* Get the package on CRAN


## Low priority

* Think of creating a fully customizable sapphire by creating layer/panel through the ggplot syntax e.g.,
```
ggsapphire(pi_data) +
geom_kinetic() +
geom_time() +
geom_structural(data, aes(..))
```